package com.epam.newsmanagment.entity;

import java.io.Serializable;

/**
 * Created by Fiodar_Kamok on 6/18/2015.
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = 5989379109847562162L;

    private Long id; /** unique parameter for every tag */
    private String tagName; /** the name of tag, different tags have different names  */

    public Tag() {
    }

    public Tag(Long id, String tagName) {
        super();
        this.id = id;
        this.tagName = tagName;
    }

    public Long getId() {
        return id;
    }
    public String getTagName() {
        return tagName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tag other = (Tag) obj;
        if (id != other.id)
            return false;
        if (tagName == null) {
            if (other.tagName != null)
                return false;
        } else if (!tagName.equals(other.tagName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Tag: id = " + id + ", tagName = " + tagName;
    }


}