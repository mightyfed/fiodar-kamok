package com.epam.newsmanagment.entity;

import java.util.List;

public class SearchCriteria {

    private List<Long> tagsId;
    private Long authorId;

    public SearchCriteria() {
    }

    public SearchCriteria(List<Long> tagsId, Long authorId) {
        this.tagsId = tagsId;
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((authorId == null) ? 0 : authorId.hashCode());
        result = prime * result + ((tagsId == null) ? 0 : tagsId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SearchCriteria other = (SearchCriteria) obj;
        if (authorId == null) {
            if (other.authorId != null)
                return false;
        } else if (!authorId.equals(other.authorId))
            return false;
        if (tagsId == null) {
            if (other.tagsId != null)
                return false;
        } else if (!tagsId.equals(other.tagsId))
            return false;
        return true;
    }

    public String toString() {
        return "SearchCriteria [tagsId=" + tagsId + ", authorId=" + authorId
                + "]";
    }




}