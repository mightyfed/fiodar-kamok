package com.epam.newsmanagment.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Fiodar_Kamok on 6/18/2015.
 */
public class Author implements Serializable {
    private static final long serialVersionUID = -5041422011598568283L;

    private Long id; /** unique parameter for every author*/
    private String name; /** name of author, different authors may have the same names*/
    private Date expired; /** time from which author hasn't publications*/

    public Author() {
    }

    public Author(Long id, String name, Date expired) {
        super();
        this.id = id;
        this.name = name;
        this.expired = expired;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getExpired() {
        return expired;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expired == null) ? 0 : expired.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Author other = (Author) obj;
        if (expired == null) {
            if (other.expired != null)
                return false;
        } else if (!expired.equals(other.expired))
            return false;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Author [id=" + id + ", name=" + name + ", expired=" + expired
                + "]";
    }


}
