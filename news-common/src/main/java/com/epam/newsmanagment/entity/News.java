package com.epam.newsmanagment.entity;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by Fiodar_Kamok on 6/18/2015.
 */
public class News implements Serializable {
    private static final long serialVersionUID = 3423313146940754553L;


    private Long id; /** unique parameter for every news */
    private String shortText; /** short text of news */
    private String fullText; /** full text of news*/
    private String title; /**title of news */
    private Date creationDate; /** date when news was created*/
    private Date modificationDate; /**date when news was modificated */

    public News() {
    }

    public News(Long id, String shortText, String fullText, String title,
                Date creationDate, Date modificationDate) {
        super();
        this.id = id;
        this.shortText = shortText;
        this.fullText = fullText;
        this.title = title;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }


    public Long getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }


    public String getShortText() {
        return shortText;
    }


    public String getFullText() {
        return fullText;
    }


    public Date getCreationDate() {
        return creationDate;
    }


    public Date getModificationDate() {
        return modificationDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result
                + ((fullText == null) ? 0 : fullText.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime
                * result
                + ((modificationDate == null) ? 0 : modificationDate.hashCode());
        result = prime * result
                + ((shortText == null) ? 0 : shortText.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        News other = (News) obj;
        if (creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!creationDate.equals(other.creationDate))
            return false;
        if (fullText == null) {
            if (other.fullText != null)
                return false;
        } else if (!fullText.equals(other.fullText))
            return false;
        if (id != other.id)
            return false;
        if (modificationDate == null) {
            if (other.modificationDate != null)
                return false;
        } else if (!modificationDate.equals(other.modificationDate))
            return false;
        if (shortText == null) {
            if (other.shortText != null)
                return false;
        } else if (!shortText.equals(other.shortText))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "News [id=" + id + ", title=" + title + ", shortText=" + shortText + ", fullText="
                + fullText +  ", creationDate="
                + creationDate + ", modificationDate=" + modificationDate + "]";
    }





}
