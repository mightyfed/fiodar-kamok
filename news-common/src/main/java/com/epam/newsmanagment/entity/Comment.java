package com.epam.newsmanagment.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Fiodar_Kamok on 6/18/2015.
 */
public class Comment implements Serializable {
    private static final long serialVersionUID = 8828245656298598088L;

    private Long id;  /**unique parameter for every comment */
    private String text; /**text of comment */
    private Date creationDate; /** date of comment creation */
    private Long newsId; /** id of news for which this comment was created*/

    public Comment() {
    }

    public Comment(Long id, String text, Date creationDate, Long newsId) {
        super();
        this.id = id;
        this.text = text;
        this.creationDate = creationDate;
        this.newsId = newsId;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + (int) (newsId ^ (newsId >>> 32));
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Comment other = (Comment) obj;
        if (creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!creationDate.equals(other.creationDate))
            return false;
        if (id != other.id)
            return false;
        if (newsId != other.newsId)
            return false;
        if (text == null) {
            if (other.text != null)
                return false;
        } else if (!text.equals(other.text))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Comment [id=" + id + ", text=" + text + ", creationDate="
                + creationDate + ", newsId=" + newsId + "]";
    }

}
