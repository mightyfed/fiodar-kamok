package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.NewsDAO;
import com.epam.newsmanagment.dao.impl.NewsDAOImpl;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



@Transactional(rollbackFor = Exception.class)
public class NewsServiceImpl implements INewsService {
	private static Logger logger = Logger.getLogger(NewsServiceImpl.class);
	private NewsDAO newsDAO;

	public NewsServiceImpl() {
	}

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public long addNews(News news) throws ServiceException {
		try {
			return newsDAO.add(news);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public boolean editNews(News news)  throws ServiceException {
		try {
			return newsDAO.edit(news);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public boolean deleteNews(Long newsId) throws ServiceException {
		try {
			return newsDAO.delete(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public boolean deleteNewsList(List<Long> newsId) throws ServiceException {
		try {
			return newsDAO.deleteNewsList(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public List<News> findByAuthor(Long authorId) throws ServiceException {
		try {
			return newsDAO.findByAuthor(authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public List<News> findAll(Long pageIndex, int numNews) throws ServiceException {
		try {
			return newsDAO.findAll(pageIndex, numNews);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public List<News> findByTags(List<Long> tagsId) throws ServiceException {
		try {
			return newsDAO.findByTags(tagsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public News findById(Long id) throws ServiceException {
		try {
			return newsDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public Long findAuthorForNews(Long newsId) throws ServiceException {
		try {
			return newsDAO.findAuthorForNews(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public List<News> findByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNews) throws ServiceException {
		try {
			return newsDAO.findNewsByFilters(searchCriteria, pageIndex, numNews);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public int countNumNews(SearchCriteria searchcriteria) throws ServiceException {
		try {
			return newsDAO.countNumNews(searchcriteria);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}


}
