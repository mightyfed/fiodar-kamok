package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.AuthorDAO;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;



@Transactional(rollbackFor = Exception.class)
public class AuthorServiceImpl implements IAuthorService {
	private static Logger logger = Logger.getLogger(AuthorServiceImpl.class);

	private AuthorDAO authorDAO;

	public AuthorServiceImpl() {
	}

	public void setAuthorDAO(AuthorDAO authorDao) {
		this.authorDAO = authorDao;
	}


	public Long addAuthor(Author author) throws ServiceException {
		try {
			return authorDAO.add(author);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public Author findAuthorById(Long id) throws ServiceException {
		try {
			return authorDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public Author findAuthorByNewsId(Long newsId) throws ServiceException {
		try {
			return authorDAO.findByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}


	public boolean attachAuthorToNews(Long newsId, Long authorId) throws ServiceException {
		try {
			return authorDAO.attachAuthorToNews(newsId, authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public boolean detachAuthorForNews(Long newsId) throws ServiceException {
		try {
			return authorDAO.detachAuthorForNews(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public boolean detachAuthorForNewsList(List<Long> newsId) throws ServiceException {
		try {
			return authorDAO.detachAuthorForNewsList(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public List<Author> findAllAuthorsNoExpired() throws ServiceException {
		try {
			return authorDAO.findAllAuthorsNoExpired();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public List<Author> findAllAuthors() throws ServiceException {
		try {
			return authorDAO.findAllAuthors();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public boolean editAuthor(Author author) throws ServiceException {
		try {
			return authorDAO.edit(author);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public boolean expireAuthor(Long authorId)
			throws ServiceException {
		try {
			Date expiredDate = new Date();
			return authorDAO.expire(authorId, expiredDate);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
}