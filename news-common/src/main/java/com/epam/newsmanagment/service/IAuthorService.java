package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;

import java.util.List;

public interface IAuthorService {
	/**
	 *
	 * inserts author into database
	 * @param author - entity we insert into database
	 * @return id of author
	 * @throws ServiceException
	 */
	public Long addAuthor(Author author) throws ServiceException;
	/**
	 *
	 * finds author by id
	 * @param id by which we find entity
	 * @return author if we found, or null if author with this id doesn't exist
	 * @throws ServiceException
	 */
	public Author findAuthorById(Long id) throws ServiceException;
	/**
	 * finds author by newsId
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public Author findAuthorByNewsId(Long newsId) throws ServiceException;
	/**
	 *
	 * attaches author to news
	 * @param newsId
	 * @param authorId
	 * @return true if author attached, else false
	 * @throws ServiceException
	 */
	public boolean  attachAuthorToNews(Long newsId, Long authorId) throws ServiceException;
	/**
	 *
	 * detaches author for news
	 * @param newsId
	 * @return true if author attached, else false
	 */
	public boolean detachAuthorForNews(Long newsId) throws ServiceException;
	/**
	 *
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public boolean detachAuthorForNewsList(List<Long> newsId) throws ServiceException;
	/**
	 * finds all authors
	 * @return
	 * @throws ServiceException
	 */
	public List<Author> findAllAuthors() throws ServiceException;
	/**
	 *
	 * finds all authors non expired
	 * @return list of authors
	 * @throws ServiceException
	 */
	public List<Author> findAllAuthorsNoExpired() throws ServiceException;
	/**
	 *
	 * edits author
	 * @param author
	 * @return
	 * @throws ServiceException
	 */
	public boolean editAuthor(Author author) throws ServiceException;
	/**
	 *
	 * makes author expired
	 * @return
	 * @throws ServiceException
	 */
	public boolean expireAuthor(Long authorId) throws ServiceException;
}