package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.entity.*;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(rollbackFor=Exception.class)
public class NewsManagerServiceImpl implements INewsManagerService {
	private final static Logger logger = Logger.getLogger(NewsManagerServiceImpl.class);

	private INewsService newsService;
	private IAuthorService authorService;
	private ITagService tagService;
	private ICommentService commentService;

	public NewsManagerServiceImpl() {
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public INewsService getNewsService() {
		return newsService;
	}

	public IAuthorService getAuthorService() {
		return authorService;
	}

	public ITagService getTagService() {
		return tagService;
	}

	public ICommentService getCommentService() {
		return commentService;
	}

	public Long saveNews(News news, Long authorId, List<Long> tags) throws ServiceException {
		if(news.getId() == 0) {
			return addNews(news, authorId, tags);
		} else {
			editNews(news, authorId, tags);
			return news.getId();
		}

	}

	private Long addNews(News news, Long authorId, List<Long> tags) throws ServiceException {
		try {
			Long id = newsService.addNews(news);
			tagService.attachTagsToNews(tags, id);
			authorService.attachAuthorToNews(id, authorId);
			return id;
		} catch (ServiceException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	private void editNews(News news, Long authorId, List<Long> tags) throws ServiceException {
		try {
			newsService.editNews(news);
			tagService.detachTagsForNews(news.getId());
			tagService.attachTagsToNews(tags, news.getId());
			authorService.detachAuthorForNews(news.getId());
			authorService.attachAuthorToNews(news.getId(), authorId);
		} catch (ServiceException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public void deleteNews(List<Long> newsId) throws ServiceException {
		try {
			commentService.deleteCommentsForNewsList(newsId);
			tagService.detachTagsForNewsList(newsId);
			authorService.detachAuthorForNewsList(newsId);
			newsService.deleteNewsList(newsId);
		} catch (ServiceException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public NewsVO findNewsVOById(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		News news = newsService.findById(newsId);
		Author author = authorService.findAuthorByNewsId(newsId);
		List<Comment> comments = commentService.findCommentsForNews(newsId);
		List<Tag> tags = tagService.findTagsForNews(newsId);
		newsVO.setNews(news);
		newsVO.setAuthor(author);
		newsVO.setComments(comments);
		newsVO.setTags(tags);
		return newsVO;
	}

	public List<NewsVO> findNewsVOList(SearchCriteria searchCriteria, int pageIndex, int numNews) throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		List<News> newsList = newsService.findByFilters(searchCriteria, pageIndex, numNews);
		for(News news : newsList) {
			Author author = authorService.findAuthorByNewsId(news.getId());
			List<Tag> tags = tagService.findTagsForNews(news.getId());
			List<Comment> comments = commentService.findCommentsForNews(news.getId());
			NewsVO newsVO = new NewsVO(news, author, tags, comments);
			newsVOList.add(newsVO);
		}

		return newsVOList;
	}


}