package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.User;
import com.epam.newsmanagment.exception.ServiceException;

public interface IUserService {

	/**
	 * 
	 * @param login
	 * @return user
	 * @throws ServiceException
	 */
	User findUserByLogin(String login) throws ServiceException;
	
	
	
}
