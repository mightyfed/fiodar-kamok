package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.ServiceException;

import java.util.List;

public interface ICommentService {
	/**
	 *
	 * inserts comment into database
	 * @param comment - entity we insert into database
	 * @return id of inserted entity
	 * @throws ServiceException
	 */
	public Long addComment(Comment comment) throws ServiceException;
	/**
	 *
	 * deletes single comment
	 * @param id - id of deleted comment
	 * @return true if comment deleted, else false
	 * @throws ServiceException
	 */
	public boolean deleteComment(Long commentId) throws ServiceException;
	/**
	 * deletes comments by news id
	 * @param idNews - id by which we delete all comments
	 * @return true if comments for this news deleted, else false
	 * @throws ServiceException
	 */
	public boolean deleteCommentsForNewsList(List<Long> newsIdnewsIdList) throws ServiceException;
	/**
	 * finds comments by news id
	 * @param idNews - id by which method finds comments
	 * @return list of comments
	 * @throws ServiceException
	 */
	public List<Comment> findCommentsForNews(Long newsId) throws ServiceException;
}
