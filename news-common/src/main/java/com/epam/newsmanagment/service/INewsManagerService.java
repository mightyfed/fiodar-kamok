package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.*;
import com.epam.newsmanagment.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

public interface INewsManagerService {

	void setNewsService(INewsService newsService) ;

	void setAuthorService(IAuthorService authorService) ;

	void setTagService(ITagService tagService);

	void setCommentService(ICommentService commentService) ;

	INewsService getNewsService() ;

	IAuthorService getAuthorService() ;

	ITagService getTagService();

	ICommentService getCommentService() ;

	Long saveNews(News news, Long authorId, List<Long> tags) throws ServiceException ;


	void deleteNews(List<Long> newsId) throws ServiceException ;

	NewsVO findNewsVOById(Long newsId) throws ServiceException ;

	List<NewsVO> findNewsVOList(SearchCriteria searchCriteria, int pageIndex, int numNews) throws ServiceException ;


}
