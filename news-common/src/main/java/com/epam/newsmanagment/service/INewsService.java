package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;

import java.util.List;

public interface INewsService {
	/**
	 * adds news
	 * @param news inserted into database
	 * @return id of inserted news
	 * @throws ServiceException
	 */
	public long addNews (News news) throws ServiceException;
	/**
	 * edits news
	 * @param news - edited news
	 * @return true if news edited, else false
	 * @throws ServiceException
	 */
	public boolean editNews(News news) throws ServiceException;
	/**
	 * deletes news by id
	 * @param id of deleted news
	 * @return true if news deleted, else return false
	 * @throws ServiceException
	 */
	public boolean deleteNews (Long newsId) throws ServiceException;
	/**
	 *
	 * @param newsId
	 * @return true if news deleted, else return false
	 * @throws ServiceException
	 */
	public boolean deleteNewsList(List<Long> newsId) throws ServiceException;
	/**
	 * finds author id by news id
	 * @param newsId - id by which method finds author id
	 * @return author id
	 * @throws ServiceException
	 */
	public Long findAuthorForNews(Long newsId) throws ServiceException;
	/**
	 * finds news by author
	 * @param author - for which we find news
	 * @return list of news for this author, list will be empty if author has't news
	 * @throws ServiceException
	 */
	public List<News> findByAuthor(Long authorId) throws ServiceException;
	/**
	 * finds all news
	 * @param pageIndex - index of page we found,
	 * @param numNews - number of news we found
	 * @return list of news sorted by comments and modification date
	 * @throws ServiceException
	 */
	public List<News> findAll(Long pageIndex, int numNews) throws ServiceException;
	/**
	 * finds news by tags
	 * @param tags
	 * @return list of news finded by tags, if there no news with this tags, method return empty list
	 * @throws ServiceException
	 */
	public List<News> findByTags(List<Long> tagsId) throws ServiceException;
	/**
	 * finds single news by id
	 * @param id by which we find news
	 * @return news, all it properties
	 * @throws ServiceException
	 */
	public News findById(Long id)  throws ServiceException;
	/**
	 *
	 * finds List of news by some criteria
	 * @param searchCriteria TODO
	 * @param pageIndex
	 * @param numNews
	 * @param tags - list of tags
	 * @param author
	 * @return list of news with this tags and author
	 * @throws ServiceException
	 */
	public List<News> findByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNews) throws ServiceException;
	/**
	 *
	 * finds number of news
	 * @param searchcriteria TODO
	 * @return
	 * @throws ServiceException
	 */
	public int countNumNews(SearchCriteria searchcriteria) throws ServiceException;

}
