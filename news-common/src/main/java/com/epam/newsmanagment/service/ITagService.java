package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.ServiceException;

import java.util.List;

public interface ITagService {
	/**
	 *
	 * @param newsId by which we delete tags
	 * @return true if tags deleted, else return false
	 * @throws ServiceException
	 */
	public boolean detachTagsForNews(Long newsId) throws ServiceException;
	/**
	 *
	 * @param newsIdList
	 * @return true if tags deleted, else return false
	 * @throws ServiceException
	 */
	public boolean detachTagsForNewsList(List<Long> newsIdList) throws ServiceException;
	/**
	 *
	 * find tags for news
	 * @param newsId - id by which method finds tags
	 * @return list of tags
	 * @throws ServiceException
	 */
	public List<Tag> findTagsForNews(Long newsId) throws ServiceException;
	/**
	 *
	 * finds all tags
	 * @return list of tags
	 * @throws ServiceException
	 */
	public List<Tag> findAllTags() throws ServiceException;
	/**
	 *
	 * @return true if tag edited, else false
	 * @throws ServiceException
	 */
	public boolean editTag(Tag tag) throws ServiceException;
	/**
	 *
	 * @param tagId
	 * @return
	 * @throws ServiceException
	 */
	public boolean deleteTag(Long tagId)throws ServiceException;
	/**
	 *
	 * @param tagName
	 * @throws ServiceException
	 */
	public Long addTag(Tag tag) throws ServiceException;
	/**
	 *
	 * @param tagName
	 * @return
	 * @throws ServiceException
	 */
	public Tag findTagByName(String tagName) throws ServiceException;
	/**
	 *
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	public Tag findTagById(Long tagId) throws ServiceException;

	/**
	 *
	 * @param tagsId
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	boolean attachTagsToNews(List<Long> tagsId, Long newsId) throws ServiceException;
}
