package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.TagDAO;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



@Transactional(rollbackFor=Exception.class)
public class TagServiceImpl implements ITagService {
	private final static Logger logger = Logger.getLogger(TagServiceImpl.class);

	private TagDAO tagDAO;

	public TagServiceImpl() {
	}

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	@Override
	public boolean attachTagsToNews(List<Long> tagsId, Long newsId) throws ServiceException {
		try {
			return tagDAO.attachTagsToNews(tagsId, newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean detachTagsForNews(Long idNews) throws ServiceException {
		try {
			return tagDAO.detachTagsForNews(idNews);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean detachTagsForNewsList(List<Long> newsIdList) throws ServiceException {
		try {
			return tagDAO.detachTagsForNewsList(newsIdList);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> findTagsForNews(Long newsId) throws ServiceException {
		try {
			return tagDAO.findByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> findAllTags() throws ServiceException {
		try {
			return tagDAO.findAll();
		} catch(DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean editTag(Tag tag) throws ServiceException {
		try {
			return tagDAO.edit(tag);
		} catch(DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean deleteTag(Long tagId) throws ServiceException {
		try {
			tagDAO.detachTagForAllNews(tagId);
			return tagDAO.delete(tagId);
		} catch(DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Long addTag(Tag tag) throws ServiceException {
		try {
			return tagDAO.add(tag);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Tag findTagByName(String tagName) throws ServiceException {
		try {
			if(tagName != null) {
				return tagDAO.findByName(tagName);
			} else {
				throw new ServiceException("tag is null");
			}
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Tag findTagById(Long tagId) throws ServiceException {
		try {
			return tagDAO.findById(tagId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}






}
