package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.CommentDAO;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ICommentService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional(rollbackFor = Exception.class)
public class CommentServiceImpl implements ICommentService {
	private final static Logger logger = Logger.getLogger(CommentServiceImpl.class);

	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Override
	public Long addComment(Comment comment) throws ServiceException {
		try {
			return commentDAO.add(comment);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean deleteComment(Long commentId) throws ServiceException {;
		try {
			return commentDAO.delete(commentId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean deleteCommentsForNewsList(List<Long> newsIdList) throws ServiceException {
		try {
			return commentDAO.deleteByNewsIdList(newsIdList);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Comment> findCommentsForNews(Long newsId) throws ServiceException {
		try {
			return commentDAO.findCommentsByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}



}
