package com.epam.newsmanagment.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagment.dao.UserDAO;
import com.epam.newsmanagment.entity.User;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IUserService;

public class UserServiceImpl implements IUserService {
	private final static Logger logger = Logger.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDAO userDAO;
		
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	@Override
	public User findUserByLogin(String login) throws ServiceException {
		try {
			return userDAO.findUserByLogin(login);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException();
		}	
	}

}
