package com.epam.newsmanagment.utility;

import com.epam.newsmanagment.constant.SchemaSQL;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.exception.DAOException;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetCreator {
	public static News createNews(ResultSet rs) throws DAOException {
		try {
			Long id = rs.getLong(SchemaSQL.NEWS_ID);
			String shortText = rs.getString(SchemaSQL.NEWS_SHORT_TEXT);
			String fullText = rs.getString(SchemaSQL.NEWS_FULL_TEXT);
			String title = rs.getString(SchemaSQL.NEWS_TITLE);
			Date creationDate = rs.getDate(SchemaSQL.NEWS_CREATION_DATE);
			Date modificationDate = rs.getDate(SchemaSQL.NEWS_MODIFICATION_DATE);
			News entity = new News(id, shortText, fullText, title, creationDate, modificationDate);
			return entity;
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}


	public static Integer calculatePageCount(ResultSet rs, final int NEWS_PER_PAGE)
			throws DAOException {
		try {
			Integer totalCount = rs.getInt(SchemaSQL.TOTAL_COUNT);
			Integer pageCount = null;
			if (totalCount != null) {
				pageCount = (int) Math.ceil(totalCount * 1.0 / NEWS_PER_PAGE);
			}
			return pageCount;
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
}
