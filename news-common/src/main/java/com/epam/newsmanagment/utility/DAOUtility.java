package com.epam.newsmanagment.utility;

import java.sql.Statement;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagment.exception.DAOException;

public class DAOUtility {

	public static void closeResources(DataSource dataSource,
			Connection connection, PreparedStatement preparedStatement)
			throws DAOException {
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				DataSourceUtils.releaseConnection(connection, dataSource);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}

	public static void closeResources(DataSource dataSource,
			Connection connection, PreparedStatement preparedStatement,
			ResultSet resultSet) throws DAOException {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			closeResources(dataSource, connection, preparedStatement);
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}

	public static void closeResources(DataSource dataSource,
									  Connection connection, Statement st,
									  ResultSet resultSet) throws DAOException {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (st != null) {
				st.close();
			}
			if (connection != null) {
				DataSourceUtils.releaseConnection(connection, dataSource);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}

	public static void closeResources(DataSource dataSource,
			Connection connection, CallableStatement callableStatement)
			throws DAOException {
		try {
			if (callableStatement != null) {
				callableStatement.close();
			}
			if (connection != null) {
				DataSourceUtils.releaseConnection(connection, dataSource);
			}
		} catch (SQLException ex) {
			throw new DAOException(ex);
		}
	}
}
