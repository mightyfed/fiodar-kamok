package com.epam.newsmanagment.exception;

/**
 * Created by Fiodar_Kamok on 6/18/2015.
 */
public class DAOException extends Exception {
    
    private static final long serialVersionUID = 2L;

    public DAOException() {
        super();
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
