package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.NewsDAO;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utility.DAOUtility;
import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class NewsDAOImpl implements NewsDAO {

		private final static Logger logger = Logger.getLogger(NewsDAOImpl.class);

	private final static String INSERT_NEWS = "INSERT INTO NEWS(news_id, short_text, full_text, title, creation_date, modification_date) "
			+ 		"VALUES(NEWS_SEQ.NEXTVAL,?,?,?,?,?)";
	private final static String DELETE_NEWS = "DELETE FROM NEWS WHERE news_id=?";
	private final static String UPDATE_NEWS = "UPDATE NEWS SET short_text=?, full_text=?, title=?, modification_date=? "
			+ 		"WHERE news_id=?";
	private final static String FIND_ALL_NEWS = "SELECT news_id, short_text, title, creation_date, modification_date FROM ( "
			+		"SELECT news_id, total_message, short_text, title, creation_date, modification_date, rownum rn FROM ( "
			+		"SELECT news.news_id, news.short_text, news.title, news.creation_date, news.modification_date, "
			+		"COUNT (comments.comment_id) as total_message "
			+		"FROM NEWS LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID= NEWS.NEWS_ID "
			+		"GROUP BY NEWS.news_id, NEWS.short_text, NEWS.full_text, NEWS.title, "
			+		"NEWS.creation_date, NEWS.modification_date ORDER BY COUNT(COMMENTS.comment_id) DESC, modification_date ) "
			+		") WHERE rn BETWEEN ? AND ?";
	private final static String FIND_NEWS_BY_ID = "SELECT NEWS.news_id, NEWS.short_text, NEWS.full_text, NEWS.title, NEWS.creation_date, NEWS.modification_date FROM NEWS WHERE NEWS.news_id= ?";
	private final static String FIND_NEWS_BY_AUTHOR_ID = "SELECT NEWS.news_id, NEWS.short_text, NEWS.title, NEWS.creation_date, NEWS.modification_date "
			+ 		"FROM NEWS INNER JOIN NEWS_AUTHOR ON NEWS.news_id=NEWS_AUTHOR.news_id "
            +       "WHERE NEWS_AUTHOR.author_id=?";
	private final static String FIND_AUTHOR_ID_BY_NEWS_ID = "SELECT author_id FROM NEWS_AUTHOR WHERE news_id=?";
	private final static String FIND_NEWS_BY_TAGS = "SELECT news.news_id, news.short_text, news.title"
			+ 		" news.creation_date, news.modification_date"
			+		" FROM news JOIN news_tag on news_tag.news_id = news.news_id "
			+		" WHERE news_tag.tag_id in(?) GROUP BY news.news_id, news.short_text, news.title"
			+ 		" news.creation_date, news.modification_date HAVING count(*) = ?";





	private DataSource dataSource;

	public NewsDAOImpl() {
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long add(News news) throws DAOException {
		if(news == null) {
			throw new DAOException("news is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Long insertedId = 0L;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			String []id = {"news_id"};
 			pst = connection.prepareStatement(INSERT_NEWS, id);
			pst.setString(1, news.getShortText());
			pst.setString(2, news.getFullText());
			pst.setString(3, news.getTitle());
			pst.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			pst.setTimestamp(5, new Timestamp(news.getModificationDate().getTime()));
			pst.execute();
			rs = pst.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst, rs);
		}
		return insertedId;
	}

	@Override
	public boolean edit(News news) throws DAOException {
		if(news == null) {
			throw new DAOException("news is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowUpdated = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(UPDATE_NEWS);
			pst.setString(1, news.getShortText());
			pst.setString(2, news.getFullText());
			pst.setString(3, news.getTitle());
			pst.setTimestamp(4, new Timestamp(news.getModificationDate().getTime()));
			pst.setLong(5, news.getId());
			rowUpdated = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst);
		}
		return rowUpdated == 1;
	}

	@Override
	public boolean delete(Long newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(DELETE_NEWS);
			pst.setLong(1, newsId);
			rowDeleted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst);
		}
		return rowDeleted == 1;
	}

	@Override
	public boolean deleteNewsList(List<Long> newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			StringBuilder deleteNewsList = new StringBuilder("DELETE FROM NEWS WHERE news_id IN (");
			for(int i = 0; i < newsId.size()-1; i++) {
				deleteNewsList.append("?, ");
			}
			deleteNewsList.append(" ?)");
			pst = connection.prepareStatement(deleteNewsList.toString());
			for(int i = 0; i < newsId.size(); i++) {
				pst.setLong(i+1, newsId.get(i));
			}
			rowDeleted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst);
		}
		return rowDeleted > 0;
	}

	@Override
	public News findById(Long newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		News news = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_NEWS_BY_ID);
			pst.setLong(1, newsId);
			ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				String shrtText = rs.getString(2);
				String fullText = rs.getString(3);
				String title = rs.getString(4);
				Timestamp creationdate = rs.getTimestamp(5);
				Timestamp modificationDate = rs.getTimestamp(6);
				news = new News(newsId, shrtText, fullText, title, creationdate, modificationDate);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst);
		}
		return news;
	}


	@Override
	public List<News> findAll(Long pageIndex, int numNews) throws DAOException {
		PreparedStatement pst = null;
		Connection connection = null;
		List<News> news = new ArrayList<News>();
		ResultSet rs = null;
		Long startIndex, endIndex;
		if(pageIndex != 1) {
			startIndex = (pageIndex-1) * numNews + 1;
			endIndex = startIndex + numNews - 1;
		} else {
			startIndex = 1L;
			endIndex = (long)numNews;
		}

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_ALL_NEWS);
			pst.setLong(1, startIndex);
			pst.setLong(2, endIndex);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long newsId = rs.getLong(1);
				String shrtText = rs.getString(2);
				String title = rs.getString(3);
				Timestamp creationdate = rs.getTimestamp(4);
				Timestamp modificationDate = rs.getTimestamp(5);
				news.add(new News(newsId, shrtText, null, title, creationdate, modificationDate));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst, rs);
		}
		return news;
	}


	@Override
	public List<News> findByAuthor(Long authorId) throws DAOException {
		if(authorId == null) {
			throw new DAOException("authorId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		List<News> news = new ArrayList<News>();
		ResultSet rs = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_NEWS_BY_AUTHOR_ID);
			pst.setLong(1, authorId);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long newsId = rs.getLong(1);
				String shortText = rs.getString(2);
				String title = rs.getString(3);
				Timestamp creationdate = rs.getTimestamp(4);
				Timestamp modificationDate = rs.getTimestamp(5);
				news.add(new News(newsId, shortText, null, title, creationdate, modificationDate));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst, rs);
		}
		return news;
	}

	@Override
	public Long findAuthorForNews(Long newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Long authorId = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_AUTHOR_ID_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if(rs.next()) {
				authorId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst, rs);
		}
		return authorId;
	}

	@Override
	public List<News> findByTags(List<Long> tagsId) throws DAOException {
		PreparedStatement pst = null;
		Connection connection = null;
		List<News> news = new ArrayList<News>();
		ResultSet rs = null;
		try {
			StringBuilder findNewsByTags = new StringBuilder("SELECT NEWS.news_id, NEWS.short_text, NEWS.title,"
							+ 		" NEWS.creation_date, NEWS.modification_date"
							+		" FROM NEWS JOIN NEWS_TAG on NEWS_TAG.news_id = NEWS.news_id "
							+		" WHERE NEWS_TAG.tag_id IN(");
			for(int i = 0; i < tagsId.size()-1; i++) {
				findNewsByTags.append(tagsId.get(i) + ", ");
			}
			findNewsByTags.append(tagsId.get(tagsId.size()-1) + ") GROUP BY NEWS.news_id, NEWS.short_text, NEWS.title,"
					+ 		" NEWS.creation_date, NEWS.modification_date HAVING count(*)=" +tagsId.size());

			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(findNewsByTags.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				Long newsId = rs.getLong(1);
				String shrtText = rs.getString(2);
				String title = rs.getString(3);
				Timestamp creationdate = rs.getTimestamp(4);
				Timestamp modificationDate = rs.getTimestamp(5);
				news.add(new News(newsId, shrtText, null, title, creationdate, modificationDate));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst, rs);
		}
		return news;
	}

	@Override
	public List<News> findNewsByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNews)
			throws DAOException {
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagsId = searchCriteria.getTagsId();
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		List<News> news = new ArrayList<News>();
		Long startIndex, endIndex;
		if(pageIndex != 1) {
			startIndex = (long) ((pageIndex-1) * numNews + 1);
			endIndex = startIndex + numNews - 1;
		} else {
			startIndex = 1L;
			endIndex = (long)numNews;
		}
		try {
			StringBuilder findNews = new StringBuilder("SELECT newsId, newsShortText, newsTitle, newsCreationDate, "
							+ 		"newsModificationDate FROM ( "
							+		"SELECT newsId, total_message, newsShortText, newsTitle, newsCreationDate, "
							+ 		"newsModificationDate, rownum rn FROM ( "
							+ 		"SELECT newsId, newsShortText, newsTitle, newsCreationDate, newsModificationDate, "
							+		"COUNT (COMMENTS.comment_id) as total_message  FROM ( "
							+		"SELECT newsId, newsShortText, newsTitle, newsCreationDate, "
							+		"newsModificationDate FROM ( "
							+		"SELECT NEWS.news_id newsId, NEWS.short_text newsShortText, NEWS.title newsTitle, "
							+ 		"NEWS.creation_date newsCreationDate, NEWS.modification_date newsModificationDate FROM NEWS ");

			if(authorId != null) {
				findNews.append("INNER JOIN NEWS_AUTHOR ON NEWS.news_id=NEWS_AUTHOR.news_id "
							+	"WHERE NEWS_AUTHOR.author_id=? ");

			}

			findNews.append(" ) ");

			if(tagsId != null && !tagsId.isEmpty()) {

				findNews.append(" JOIN NEWS_TAG on NEWS_TAG.news_id = newsId WHERE NEWS_TAG.tag_id IN( ");
				for(int i = 0; i < tagsId.size()-1; i++) {
					findNews.append("?, ");
				}
				findNews.append("? ) GROUP BY newsId, newsShortText, newsTitle, newsCreationDate, newsModificationDate ");
			}
			findNews.append(" ) ");
			findNews.append("LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID= newsId "
					+		"GROUP BY newsId, newsShortText, newsTitle, newsCreationDate, newsModificationDate "
					+		"ORDER BY COUNT(COMMENTS.comment_id) DESC, newsModificationDate  DESC )   ) "
					+		"WHERE rn BETWEEN ? AND ? ");


			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(findNews.toString());
			int insertParam = 1;
			if(authorId != null) {
				pst.setLong(insertParam, authorId);
				insertParam++;
			}
			if(tagsId != null && !tagsId.isEmpty()) {
				for(int i = 0; i < tagsId.size(); i++) {
					pst.setLong(insertParam, tagsId.get(i));
					insertParam++;
				}
			}
			pst.setLong(insertParam, startIndex);
			insertParam++;
			pst.setLong(insertParam, endIndex);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long newsId = rs.getLong(1);
				String shortText = rs.getString(2);
				String title = rs.getString(3);
				Timestamp creationDate = rs.getTimestamp(4);
				Timestamp modificationDate = rs.getTimestamp(5);
				news.add(new News(newsId, shortText, null, title, creationDate, modificationDate));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst, rs);
		}
		return news;
	}

	@Override
	public int countNumNews(SearchCriteria searchCriteria) throws DAOException {
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagsId = searchCriteria.getTagsId();
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		int newsNum = 0;
		try {
			StringBuilder countNumOfNews = new StringBuilder("SELECT COUNT(newsId) FROM ( "
							+ 		"SELECT newsId FROM ( "
							+		"SELECT NEWS.news_id newsId FROM NEWS ");

			if(authorId != null) {
				countNumOfNews.append("INNER JOIN NEWS_AUTHOR ON NEWS.news_id=NEWS_AUTHOR.news_id "
							+		"WHERE NEWS_AUTHOR.author_id=? ");
			}
			countNumOfNews.append(") ");
			if(tagsId != null && !tagsId.isEmpty()) {

				countNumOfNews.append(" JOIN NEWS_TAG on NEWS_TAG.news_id = newsId WHERE NEWS_TAG.tag_id IN( ");
				for(int i = 0; i < tagsId.size()-1; i++) {
					countNumOfNews.append("?, ");
				}
				countNumOfNews.append("? ) GROUP BY newsId");
			}
			countNumOfNews.append(") ");

			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(countNumOfNews.toString());
			int insertParam = 1;
			if(authorId != null) {
				pst.setLong(insertParam, authorId);
				insertParam++;
			}
			if(tagsId != null && !tagsId.isEmpty()) {
				for(int i = 0; i < tagsId.size(); i++) {
					pst.setLong(insertParam, tagsId.get(i));
					insertParam++;
				}
			}
			rs = pst.executeQuery();
			if(rs.next()) {
				newsNum = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection ,pst, rs);
		}

		return newsNum;
	}



}
