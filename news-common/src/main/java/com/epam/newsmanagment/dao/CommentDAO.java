package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.DAOException;

import java.util.List;

public interface CommentDAO {
	/**
	 *
	 * inserts comment into table, connect this comment with necessary news
	 * @param comment - entity, which we insert into table
	 * @return id of inserted comment
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Long add(Comment comment) throws DAOException ;
	/**
	 *
	 * delete comment
	 * @param commentId
	 * @return true if object deleted, else false
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public boolean delete(Long commentId) throws DAOException ;
	/**
	 *
	 * delete all comments for news by newsId
	 * @param newsId - id of comments for which delete all comments
	 * @return true if all comments for news deleted, else false
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public boolean deleteByNewsIdList(List<Long> newsId) throws DAOException;
	/**
	 *
	 * find comment by id
	 * @param commentId - id of found comment
	 * @return null if comment not found, else return comment entity
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Comment findById(Long commentId) throws DAOException;
	/**
	 *
	 * finds comments by newsId
	 * @param newsId - id by which method finds comments
	 * @return list of comments
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public List<Comment> findCommentsByNewsId(Long newsId) throws DAOException;
}

