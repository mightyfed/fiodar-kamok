package com.epam.newsmanagment.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagment.dao.UserDAO;
import com.epam.newsmanagment.utility.DAOUtility;
import com.epam.newsmanagment.entity.User;
import com.epam.newsmanagment.exception.DAOException;

public class UserDAOImpl implements UserDAO {
	private final static Logger logger = Logger.getLogger(UserDAOImpl.class);
	
	public final static String ADD_USER = "INSERT INTO \"USER\"(user_id, user_name, login, password) VALUES(USER_SEQ.NEXTVAL,?,?,?)";
	public final static String FIND_USER_BY_ID = "SELECT \"USER\".user_id, user_name, login, password, role_name FROM \"USER\" "
					+ 	"INNER JOIN ROLES ON \"USER\".user_id = ROLES.user_id  WHERE \"USER\".user_id=?";
	public final static String FIND_USER_BY_LOGIN_AND_PASSWORD = "SELECT \"USER\".user_id, user_name, login, password, role_name FROM "
					+ 	"\"USER\" JOIN ROLES ON \"USER\".user_id = ROLES.user_id WHERE login=? AND password=?";
	public final static String FIND_USER_BY_LOGIN = "SELECT \"USER\".user_id, user_name, login, password, ROLES.role_name FROM \"USER\" JOIN ROLES ON \"USER\".user_id = ROLES.user_id WHERE login=?";
	public final static String ATTACH_ROLE_TO_AUTHOR = "INSERT INTO ROLES(user_id, role_name) VALUES(?,?)";
	public final static String DETACH_ROLE_FOR_AUTHOR = "DELETE FROM ROLES WHERE user_id=?";
	
	
	private DataSource dataSource;
	
	public UserDAOImpl() {
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	
	@Override
	public User findById(Long id) throws DAOException {
		if(id == null) {
			throw new DAOException("id is null");
		}
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;
		try {
			cn = DataSourceUtils.getConnection(dataSource);
			pst = cn.prepareStatement(FIND_USER_BY_ID);
			pst.setLong(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				String name = rs.getString(2);
				String login = rs.getString(3);
				String password = rs.getString(4);
				String role = rs.getString(5);
				user = new User(id, name, login, password, role);
			}
		} catch(SQLException e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		} finally {
			DAOUtility.closeResources(dataSource, cn, pst, rs);
		}
		return user;
	}

	@Override
	public Long add(User user) throws DAOException {
		if(user == null) {
			throw new DAOException("user is null");
		}
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long insertedId = 0L;
		
		try {
			cn = DataSourceUtils.getConnection(dataSource);
			String[] id = {"user_id"};
			pst = cn.prepareStatement(ADD_USER, id);
			pst.setString(1, user.getName());
			pst.setString(2, user.getLogin());
			pst.setString(3, user.getPassword());
			pst.execute();
			rs = pst.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getLong(1);
			}
		} catch(SQLException e) {
			logger.error(e);
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, cn, pst, rs);
		}
		return insertedId;
	}
	
	@Override
	public User findUserByLogin(String login) throws DAOException {
		if(login == null || login.isEmpty() ) {
			throw new DAOException("login is empty");
		}
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;

		try {
			cn = DataSourceUtils.getConnection(dataSource);
			pst = cn.prepareStatement(FIND_USER_BY_LOGIN);
			pst.setString(1, login);
			rs = pst.executeQuery();
			if(rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				String password = rs.getString(4);
				String role = rs.getString(5);
				user = new User(id, name, login, password, role);
			}
		} catch(SQLException e) {
			logger.error(e);
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, cn, pst, rs);
		}
		return user;
	}

	@Override
	public User findUserByLoginPassword(String login, String password)
			throws DAOException {
		if(login == null || password == null || login.isEmpty() || password.isEmpty()) {
			throw new DAOException("password or login empty");
		}
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		User user = null;
		try {
			cn = DataSourceUtils.getConnection(dataSource);
			pst = cn.prepareStatement(FIND_USER_BY_LOGIN_AND_PASSWORD);
			pst.setString(1, login);
			pst.setString(2, password);
			rs = pst.executeQuery();
			if(rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				String role = rs.getString(4);
				user = new User(id, name, login, password, role);
			}
		} catch(SQLException e) {
			logger.error(e);
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, cn, pst, rs);
		}
		return user;
	}

	@Override
	public boolean attachRoleToUser(Long userId, String roleName) throws DAOException {
		if(userId == null || roleName == null || roleName.isEmpty()) {
			throw new DAOException("userId is null or role name is empty");
		}
		Connection cn = null;
		PreparedStatement pst = null;
		int rowInserted = 0;
		try {
			cn = DataSourceUtils.getConnection(dataSource);
			pst = cn.prepareStatement(ATTACH_ROLE_TO_AUTHOR);
			pst.setLong(1, userId);
			pst.setString(2, roleName);
			rowInserted = pst.executeUpdate();
		} catch(SQLException e) {
			logger.error(e);
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, cn, pst);
		}
		
		
		return rowInserted == 1;
	}

	@Override
	public boolean detachRoleForUser(Long userId) throws DAOException {
		if(userId == null) {
			throw new DAOException("userId is null");
		} 
		Connection cn = null;
		PreparedStatement pst = null;
		int rowDeleted = 0;
		try {
			cn = DataSourceUtils.getConnection(dataSource);
			pst = cn.prepareStatement(DETACH_ROLE_FOR_AUTHOR);
			pst.setLong(1, userId);
			rowDeleted = pst.executeUpdate();
		} catch(SQLException e) {
			logger.error(e);
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, cn, pst);
		}
		
		return rowDeleted == 1;
	}

	

	
	
}
