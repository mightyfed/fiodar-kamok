package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;

import java.util.List;

public interface NewsDAO {
	/**
	 *
	 * inserts news into database
	 * @param news - entity we insert into database
	 * @param authorId - id of news Author
	 * @return long id of inserted value
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Long add(News news) throws DAOException;
	/**
	 *
	 * Updates <code>News</code> entity
	 * @param news - edited news, method changes some properties of news entity
	 * @return true if news edited, else return false
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public boolean edit(News news) throws DAOException;
	/**
	 *
	 * deletes news by id
	 * @param id of news, by which method deletes entity
	 * @return true, if news was deleted, otherwise - false
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public boolean delete(Long id) throws DAOException;
	/**
	 *
	 * @param newsId
	 * @return true, if news was deleted, otherwise - false
	 * @throws DAOException
	 */
	public boolean deleteNewsList(List<Long> newsId) throws DAOException;
	/**
	 *
	 * finds author id by news id
	 * @param id of news, by which method finds author id
	 * @return author id
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Long findAuthorForNews(Long newsId) throws DAOException;
	/**
	 *
	 * finds news by id
	 * @param id of news by which we find entity, every news has unique id
	 * @return news by our id
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public News findById(Long id) throws DAOException;
	/**
	 *
	 * finds all news sorted by comments number and then by modification date, and take only concrete number of news from start index;
	 * @param pageIndex - start index of page we select
	 * @param numNews - number of news we select
	 * @return list of news we selected
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public List<News> findAll(Long pageIndex, int numNews) throws DAOException;
	/**
	 *
	 * finds news for author
	 * @param author - entity for which we find news
	 * @return list of news from author
	 * @throws DAOException
	 */
	public List<News> findByAuthor(Long authorId) throws DAOException;
	/**
	 *
	 * finds news for tags
	 * @param tags - list of tags
	 * @return all news with this tags
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public List<News> findByTags(List<Long> tagsId) throws DAOException;
	/**
	 *
	 * finds news by filters
	 * @param searchCriteria TODO
	 * @return list of news
	 * @throws DAOException
	 */
	public List<News> findNewsByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNews) throws DAOException;
	/**
	 *
	 * finds number of news for some selection
	 * @param searchCriteria TODO
	 * @return
	 * @throws DAOException
	 */
	public int countNumNews(SearchCriteria searchCriteria) throws DAOException;


}
