package com.epam.newsmanagment.dao;


import com.epam.newsmanagment.entity.User;
import com.epam.newsmanagment.exception.DAOException;

public interface UserDAO {
	/**
	 * 
	 * @param id
	 * @return user 
	 * @throws DAOException
	 */
	public User findById(Long id) throws DAOException; 
	/**
	 * 
	 * @param user
	 * @return id of added user
	 * @throws DAOException
	 */
	public Long add(User user) throws DAOException;  
	/**
	 * 
	 * @param login
	 * @return user
	 * @throws DAOException
	 */
	public User findUserByLogin(String login) throws DAOException; 
	/**
	 * 
	 * @param login
	 * @param password
	 * @return user
	 * @throws DAOException
	 */
	public User findUserByLoginPassword(String login, String password) throws DAOException;
	/**
	 * 
	 * @param userId
	 * @param roleName
	 * @return true if role attached, else false
	 * @throws DAOException
	 */
	public boolean attachRoleToUser(Long userId, String roleName) throws DAOException;
	/**
	 * 
	 * @param userId
	 * @return true if role detached, else false
	 * @throws DAOException
	 */
	public boolean detachRoleForUser(Long userId) throws DAOException; 
}
