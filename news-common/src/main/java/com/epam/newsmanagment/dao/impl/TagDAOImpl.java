package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.constant.SchemaSQL;
import com.epam.newsmanagment.dao.TagDAO;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utility.DAOUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TagDAOImpl implements TagDAO {

	private final static Logger logger = Logger.getLogger(TagDAOImpl.class);


	private final static String ADD_TAG = "INSERT INTO TAG(tag_id, tag_name) VALUES (TAG_SEQ.NEXTVAL, ?)";
	private final static String ATTACH_TAGS_FOR_NEWS = "INSERT INTO NEWS_TAG VALUES(?,?)";
	private final static String DETACHT_TAG_FOR_ALL_NEWS = "DELETE FROM NEWS_TAG WHERE tag_id=?";
	private final static String EDIT_TAG_BY_ID = "UPDATE TAG SET tag_name=? WHERE tag_id=?";
	private final static String DELETE_BY_ID = "DELETE FROM TAG WHERE tag_id=?";
	private final static String DELETE_BY_NEWS_ID = "DELETE FROM NEWS_TAG WHERE news_id=?";
	private final static String FIND_TAG_BY_ID = "SELECT tag_id, tag_name FROM TAG WHERE tag_id=?";
	private final static String FIND_TAG_BY_NAME = "SELECT tag_id, tag_name FROM TAG WHERE tag_name=?";
	private final static String FIND_BY_NEWS_ID = "SELECT tag.tag_id, tag.tag_name FROM TAG, NEWS_TAG "
			+ 		"WHERE TAG.tag_id=NEWS_TAG.tag_id AND NEWS_TAG.news_id=?";

	private final static String FIND_ALL_TAGS = "SELECT tag_id, tag_name FROM TAG";


	private DataSource dataSource;

	public TagDAOImpl() {
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Tag findById(Long tagId) throws DAOException {
		if(tagId == null) {
			throw new DAOException("tagId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Tag tag = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_TAG_BY_ID);
			pst.setLong(1, tagId);
			rs = pst.executeQuery();
			if(rs.next()) {
				String tagName = rs.getString(2);
				tag = new Tag (tagId, tagName);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return tag;
	}

	@Override
	public Tag findByName(String tagName) throws DAOException {
		if(tagName == null) {
			throw new DAOException("tagName is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Tag tag = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_TAG_BY_NAME);
			pst.setString(1, tagName);
			rs = pst.executeQuery();
			if(rs.next()) {
				Long tagId = rs.getLong(1);
				tag = new Tag (tagId, tagName);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return tag;

	}

	@Override
	public Long add(Tag entity) throws DAOException {
		if(entity == null) {
			throw new DAOException("tag is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Long insertId = 0L;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			String[] id = {"tag_id"};
			pst = connection.prepareStatement(ADD_TAG, id);
			pst.setString(1, entity.getTagName());
			pst.execute();
			rs = pst.getGeneratedKeys();
			if (rs.next()) {
				insertId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return insertId;
	}

	@Override
	public boolean attachTagsToNews(List<Long> tags, Long newsId) throws DAOException {
		if(newsId == null || tags == null || tags.isEmpty()) {
			throw new DAOException("newsId is null or tags list is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int tagAttached = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			for(Long tagsId : tags) {
				pst = connection.prepareStatement(ATTACH_TAGS_FOR_NEWS);
				pst.setLong(1, newsId);
				pst.setLong(2, tagsId);
				tagAttached = pst.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return tagAttached == 1;
	}

	@Override
	public boolean detachTagsForNews(Long newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowsDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(DELETE_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rowsDeleted = pst.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowsDeleted > 0;
	}

	@Override
	public boolean detachTagsForNewsList(List<Long> newsIdList) throws DAOException {
		if(newsIdList == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowsDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			StringBuilder detacheTagsByNewsList = new StringBuilder("DELETE FROM NEWS_TAG WHERE news_id IN( ");
			for(int i = 0; i < newsIdList.size()-1; i++) {
				detacheTagsByNewsList.append("?, ");
			}
			detacheTagsByNewsList.append(" ?)");
			pst = connection.prepareStatement(detacheTagsByNewsList.toString());
			for(int i = 0; i < newsIdList.size(); i++) {
				pst.setLong(i+1, newsIdList.get(i));
			}
			rowsDeleted = pst.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowsDeleted > 0;
	}

	@Override
	public boolean detachTagForAllNews(Long tagId) throws DAOException {
		if(tagId == null) {
			throw new DAOException("tagId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowsDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(DETACHT_TAG_FOR_ALL_NEWS);
			pst.setLong(1, tagId);
			rowsDeleted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowsDeleted > 0;
	}

	@Override
	public List<Tag> findByNewsId(Long newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("newsId is null");
		}
		List<Tag> tags = new ArrayList<Tag>();
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long tagId = rs.getLong(1);
				String tagName = rs.getString(2);
				tags.add(new Tag(tagId, tagName));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return tags;
	}

	@Override
	public List<Tag> findAll() throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_ALL_TAGS);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long tagId = rs.getLong(1);
				String tagName = rs.getString(2);
				tags.add(new Tag(tagId, tagName));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return tags;
	}

	@Override
	public boolean edit(Tag tag) throws DAOException {
		if(tag == null) {
			throw new DAOException("tag is null");
		}
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int rowUpdated = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(EDIT_TAG_BY_ID);
			pst.setString(1, tag.getTagName());
			pst.setLong(2, tag.getId());
			rowUpdated = pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return rowUpdated == 1;
	}

	@Override
	public boolean delete(Long tagId) throws DAOException {
		if(tagId == null) {
			throw new DAOException("tagId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(DELETE_BY_ID);
			pst.setLong(1, tagId);
			rowDeleted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowDeleted == 1;
	}









}
