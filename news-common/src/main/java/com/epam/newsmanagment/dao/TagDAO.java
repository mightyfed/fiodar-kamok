package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.DAOException;

import java.util.List;

public interface TagDAO {
	/**
	 *
	 * find tag by id
	 * @param tagId - unique parameter
	 * @return tag entity or null
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Tag findById (Long tagId) throws DAOException;
	/**
	 *
	 * insert tag into database
	 * @param tag - new tag we insert in table
	 * @return id of inserted tag
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Long add(Tag tag) throws DAOException;
	/**
	 *
	 * attach tags to news
	 * @param tags - list of tags we add for news
	 * @param newsId - news for which we add tags
	 * @return true if, tags added for news, else false
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public boolean attachTagsToNews(List<Long> tags, Long newsId) throws DAOException;
	/**
	 *
	 * delete all tags for news
	 * @param newsId - id of news for which we delete all tags
	 * @return true if tags detached, else false
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public boolean detachTagsForNews(Long newsId) throws DAOException;
	/**
	 *
	 * @param newsIdList
	 * @return true if tags detached, else false
	 * @throws DAOException
	 */
	public boolean detachTagsForNewsList(List<Long> newsIdList) throws DAOException;
	/**
	 *
	 * find all tags to news
	 * @param newsId - id of news for which we find tags
	 * @return list of tags for news
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public List<Tag> findByNewsId(Long newsId) throws DAOException;
	/**
	 * find tag by name
	 * @param tagName
	 * @return tag entity or null
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Tag findByName(String tagName) throws DAOException;
	/**
	 *
	 * find all tags
	 * @return list of tags
	 * @throws DAOException
	 */
	public List<Tag> findAll() throws DAOException;
	/**
	 *
	 * @param tag
	 * @return true if tag edited, else false
	 * @throws DAOException
	 */
	public boolean edit(Tag tag) throws DAOException;
	/**
	 *
	 * @param tagId
	 * @return
	 * @throws DAOException
	 */
	public boolean delete(Long tagId) throws DAOException;
	/**
	 *
	 * @param tagId
	 * @return
	 * @throws DAOException
	 */
	public boolean detachTagForAllNews(Long tagId) throws DAOException;

}