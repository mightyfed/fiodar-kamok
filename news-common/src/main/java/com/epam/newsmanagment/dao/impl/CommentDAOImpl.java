package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.constant.SchemaSQL;
import com.epam.newsmanagment.dao.CommentDAO;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utility.DAOUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CommentDAOImpl implements CommentDAO {

	private final static Logger logger = Logger.getLogger(CommentDAOImpl.class);

	private final static String ADD_COMMENT_FOR_NEWS = "INSERT INTO COMMENTS(comment_id, comment_text, creation_date, news_id) "
			+ 		"VALUES(COMMENTS_SEQ.NEXTVAL,?,?,?)";
	private final static String DELETE_COMMENT_BY_ID = "DELETE FROM COMMENTS WHERE comment_id=?";
	//	private final static String DELETE_COMMENTS_FOR_NEWS = "DELETE FROM COMMENTS WHERE news_id=?";
	private final static String FIND_BY_ID = "SELECT comment_id, comment_text, creation_date, news_id FROM COMMENTS WHERE comment_id=?";
	private final static String FIND_COMMENTS_BY_NEWS_ID = "SELECT comment_id, comment_text, creation_date, news_id FROM COMMENTS "
			+ 		"WHERE news_id=? ORDER BY creation_date DESC";

	private DataSource dataSource;

	public CommentDAOImpl() {
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Long add(Comment comment) throws DAOException {
		if(comment == null) {
			throw new DAOException("comment is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Long insertedId = 0L;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			String id[] = {"comment_id"};
			pst = connection.prepareStatement(ADD_COMMENT_FOR_NEWS, id);
			pst.setString(1, comment.getText());
			pst.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
			pst.setLong(3, comment.getNewsId());
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return insertedId;
	}

	@Override
	public boolean delete(Long commentId) throws DAOException {
		if(commentId == null) {
			throw new DAOException("comment id is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(DELETE_COMMENT_BY_ID);
			pst.setLong(1, commentId);
			rowDeleted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowDeleted == 1;
	}

	@Override
	public boolean deleteByNewsIdList(List<Long> newsIds) throws DAOException {
		if(newsIds == null) {
			throw new DAOException("news id is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowsDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			StringBuilder deleteCommentsForNews = new StringBuilder("DELETE FROM COMMENTS WHERE news_id IN(");
			for(int i = 0; i < newsIds.size()-1; i++) {
				deleteCommentsForNews.append(" ?, ");
			}
			deleteCommentsForNews.append(" ?)");
			pst = connection.prepareStatement(deleteCommentsForNews.toString());
			for(int i = 0; i < newsIds.size(); i++) {
				pst.setLong(i+1, newsIds.get(i));
			}
			rowsDeleted = pst.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowsDeleted > 0;
	}

	@Override
	public Comment findById(Long commentId) throws DAOException {
		if(commentId == null) {
			throw new DAOException("comment id is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Comment comment = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_BY_ID);
			pst.setLong(1, commentId);
			rs = pst.executeQuery();
			if(rs.next()) {
				String commentText = rs.getString(2);
				Timestamp creationDate = rs.getTimestamp(3);
				Long newsId = rs.getLong(4);
				comment = new Comment(commentId, commentText, creationDate, newsId);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return comment;
	}

	@Override
	public List<Comment> findCommentsByNewsId(Long newsId) throws DAOException {
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Comment> comments = new ArrayList<Comment>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_COMMENTS_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long commentId = rs.getLong(1);
				String commentText = rs.getString(2);
				Timestamp creationDate = rs.getTimestamp(3);
				comments.add(new Comment(commentId, commentText, creationDate, newsId));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return comments;
	}
}