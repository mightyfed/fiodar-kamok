package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.DAOException;

import java.util.Date;
import java.util.List;

public interface AuthorDAO {
	/**
	 *
	 * finds author by it's id
	 * @param id - id by which we find author in database
	 * @return Author entity, or null if author not found
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Author findById(Long authorId) throws DAOException;
	/**
	 * finds author by news id
	 * @param newId
	 * @return
	 * @throws DAOException
	 */
	public Author findByNewsId(Long newId) throws DAOException;
	/**
	 *
	 * inserts author into database, different authors may have the same name
	 * @param author - entity we insert into database
	 * @return long id - the id of Author, we insert into database
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public Long add(Author author) throws DAOException;
	/**
	 *
	 * when author stopped publish, make him expired
	 * @param id - id by which we make author expired
	 * @param expiredDate - date when author stopped publish
	 * @return true if author expired, else false
	 * @throws DAOException if connection failed, or other SQLException
	 */
	public boolean expire(Long id, Date expiredDate) throws DAOException;
	/**
	 *
	 * attaches author to news
	 * @param newsId - news to which we attach author
	 * @param authorId - id of entity, we attach to news
	 * @return true if author added to news else false
	 */
	public boolean attachAuthorToNews(Long newsId, Long authorId) throws DAOException;
	/**
	 *
	 * detaches author to news
	 * @param newsId - news to which we delete author
	 * @return true if author deleted, else false
	 */
	public boolean detachAuthorForNews(Long newsId) throws DAOException;

	/**
	 * detaches authors for news list
	 * @param newsIdList
	 * @return
	 * @throws DAOException
	 */
	public boolean detachAuthorForNewsList(List<Long> newsIdList) throws DAOException;
	/**
	 * finds all authors
	 * @return
	 * @throws DAOException
	 */
	public List<Author> findAllAuthors() throws DAOException;
	/**
	 *
	 * finds all authors no expired
	 * @return list of authors
	 * @throws DAOException
	 */
	public List<Author> findAllAuthorsNoExpired() throws DAOException;
	/**
	 *
	 * edits author info
	 * @param author
	 * @return
	 * @throws DAOException
	 */
	public boolean edit(Author author) throws DAOException;

}
