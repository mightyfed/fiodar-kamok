package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.constant.SchemaSQL;
import com.epam.newsmanagment.dao.AuthorDAO;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utility.DAOUtility;
import com.epam.newsmanagment.utility.ResultSetCreator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AuthorDAOImpl implements AuthorDAO {
	private final static Logger logger = Logger.getLogger(AuthorDAOImpl.class);

	private final static String ADD_AUTHOR = "INSERT INTO AUTHOR(author_id, author_name, expired) VALUES(AUTHOR_SEQ.NEXTVAL, ?, ?)";
	private final static String FIND_BY_ID = "SELECT author_id, author_name, expired FROM AUTHOR WHERE author_id=?";
	private final static String FIND_BY_NEWS_ID = "SELECT AUTHOR.author_id, author_name, expired FROM AUTHOR, NEWS_AUTHOR WHERE news_id=? AND NEWS_AUTHOR.author_id=AUTHOR.author_id ";
	private final static String FIND_ALL_AUTHORS_NO_EXPIRED = "SELECT author_id, author_name, expired FROM AUTHOR WHERE expired IS null";
	private final static String FIND_ALL_AUTHORS = "SELECT author_id, author_name, expired FROM AUTHOR";
	private final static String EXPIRE_BY_ID = "UPDATE AUTHOR SET expired=? WHERE author_id=?";
	private final static String ATTACH_AUTHOR_TO_NEWS = "INSERT INTO NEWS_AUTHOR(news_id, author_id) VALUES(?,?)";
	private final static String DELETE_AUTHOR_TO_NEWS = "DELETE FROM NEWS_AUTHOR WHERE news_id=?";
	private final static String UPDATE_AUTHOR = "UPDATE AUTHOR SET author_name=? WHERE author_id=?";


	private DataSource dataSource;

	public AuthorDAOImpl() {
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Author findById(Long authorId) throws DAOException {
		if(authorId == null) {
			throw new DAOException("id is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Author author = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_BY_ID);
			pst.setLong(1, authorId);
			rs = pst.executeQuery();
			if(rs.next()) {
				String authorName = rs.getString(2);
				Timestamp authorExpired = rs.getTimestamp(3);
				author = new Author(authorId, authorName, authorExpired);
			}
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return author;
	}

	@Override
	public Author findByNewsId(Long newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("id is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Author author = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_BY_NEWS_ID);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if(rs.next()) {
				Long authorId = rs.getLong(1);
				String authorName = rs.getString(2);
				Timestamp authorExpired = rs.getTimestamp(3);
				author = new Author(authorId, authorName, authorExpired);
			}
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return author;
	}

	@Override
	public Long add(Author entity) throws DAOException {
		if(entity == null) {
			throw new DAOException("author is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		Long insertedId = 0L;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			String[] id = {"author_id"};
			pst = connection.prepareStatement(ADD_AUTHOR, id);
			pst.setString(1, entity.getName());
			if(entity.getExpired() != null) {
				pst.setTimestamp(2, new Timestamp(entity.getExpired().getTime()));
			} else {
				pst.setTimestamp(2, null);
			}
			pst.execute();
			rs = pst.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getLong(1);
			}
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return insertedId;
	}

	@Override
	public boolean expire(Long authorId, Date expiredDate) throws DAOException {
		if(authorId == null || expiredDate == null) {
			throw new DAOException("author is null or expire date is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowUpdated = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(EXPIRE_BY_ID);
			pst.setTimestamp(1, new Timestamp(expiredDate.getTime()));
			pst.setLong(2, authorId);
			rowUpdated = pst.executeUpdate();
		} catch(SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowUpdated == 1;
	}

	@Override
	public boolean attachAuthorToNews(Long newsId, Long authorId) throws DAOException {
		if(authorId == null || newsId == null) {
			throw new DAOException("author is null or news is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowInserted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(ATTACH_AUTHOR_TO_NEWS);
			pst.setLong(1, newsId);
			pst.setLong(2, authorId);
			rowInserted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowInserted == 1;
	}

	@Override
	public boolean detachAuthorForNews(Long newsId) throws DAOException {
		if(newsId == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(DELETE_AUTHOR_TO_NEWS);
			pst.setLong(1, newsId);
			rowDeleted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowDeleted == 1;
	}

	@Override
	public boolean detachAuthorForNewsList(List<Long> newsIdList) throws DAOException {
		if(newsIdList == null) {
			throw new DAOException("newsId is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowDeleted = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			StringBuilder detachAuthorForNewsList = new StringBuilder("DELETE FROM NEWS_AUTHOR WHERE news_id IN(" );
			for(int i = 0; i < newsIdList.size()-1; i++) {
				detachAuthorForNewsList.append("?, ");
			}
			detachAuthorForNewsList.append(" ?)");
			pst = connection.prepareStatement(detachAuthorForNewsList.toString());
			for(int i = 0; i < newsIdList.size(); i++) {
				pst.setLong(i+1, newsIdList.get(i));
			}
			rowDeleted = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowDeleted > 0;
	}

	@Override
	public List<Author> findAllAuthorsNoExpired() throws DAOException {
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Author> authors = new ArrayList<Author>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_ALL_AUTHORS_NO_EXPIRED);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				Timestamp expired = rs.getTimestamp(3);
				authors.add(new Author(id, name, expired));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return authors;
	}

	@Override
	public List<Author> findAllAuthors() throws DAOException {
		PreparedStatement pst = null;
		Connection connection = null;
		ResultSet rs = null;
		List<Author> authors = new ArrayList<Author>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(FIND_ALL_AUTHORS);
			rs = pst.executeQuery();
			while(rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				Timestamp expired = rs.getTimestamp(3);
				authors.add(new Author(id, name, expired));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst, rs);
		}
		return authors;
	}

	@Override
	public boolean edit(Author author) throws DAOException {
		if(author == null) {
			throw new DAOException("author is null");
		}
		PreparedStatement pst = null;
		Connection connection = null;
		int rowUpdated = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			pst = connection.prepareStatement(UPDATE_AUTHOR);
			pst.setString(1, author.getName());
			pst.setLong(2, author.getId());
			rowUpdated = pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.closeResources(dataSource, connection, pst);
		}
		return rowUpdated == 1;
	}






}
