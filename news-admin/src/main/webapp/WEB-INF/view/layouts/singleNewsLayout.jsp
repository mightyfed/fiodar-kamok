<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>



<html>
	<head>
    	<title><spring:message code="title.listNews" /></title>
    	<link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet">
    	<script type="text/javascript" src="<c:url value='/resources/js/dropdown.js'/>"></script>
    	
    	<script>
     		document.addEventListener('DOMContentLoaded', onLoad, false); 
        </script>
        
	</head>
	<body>
	
		<div class="content">
		
		 
			     	<tiles:insertAttribute name="header" />
			  		
			  		
			  		<tiles:insertAttribute name="menu" />
			  		<tiles:insertAttribute name="singleNews" />
			  
					<tiles:insertAttribute name="footer" />
				  
	
			
		</div>
	
	</body>
</html>