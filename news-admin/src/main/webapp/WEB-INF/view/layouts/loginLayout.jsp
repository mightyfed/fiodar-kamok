<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>
    	<title><spring:message code="title.login" /></title>
    	<link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet">
    	 
	</head>
	<body>
	
		<div class="content login-layout">
		
		 	
			<tiles:insertAttribute name="header" />
			  		
			 <c:url value="/j_spring_security_check" var="loginUrl" />
			 <section class="login-block">
			    <form class="login-form" action="${loginUrl}" method="post">
			    	<label for="j_username" ><spring:message code="label.login"/></label>
			        <input type="text"  name="j_username" placeholder="login" required autofocus>
			       	<label for="j_password" ><spring:message code="label.password"/></label>
			        <input type="password"  name="j_password" placeholder="password" required>
			        <button type="submit"><spring:message code="button.login" /></button>
			       
			        
			    </form>
			    <c:if test="${not empty loginError}">
				    <span>
			
						<spring:message code="${loginError}" />
					</span>
				</c:if>
			 </section>
			    
			<tiles:insertAttribute name="footer" />
				  
			
		</div>
	
	</body>
</html>