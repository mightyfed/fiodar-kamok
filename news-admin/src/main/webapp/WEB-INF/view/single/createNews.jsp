<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

	<fmt:setLocale value="${pageContext.response.locale}" />	
	<spring:message code="pattern.date" var="pattern"/>	

		<div>
				
			<form method="POST" action="${contextPath}/news-admin/news/add/<c:out value="${ newsId }" />" class="newsCreation">
				
				<section class="create-news-selectors">
					<select name="authorId" required>
	   					<c:choose>
		   					<c:when test="${not empty newsWorkItem}">
		   						<option selected value="${newsWorkItem.author.id}">${newsWorkItem.author.name} </option>
		   					</c:when>
		   					<c:otherwise>
		   						<option selected value=""><spring:message code="message.author.select" /></option>
		   					</c:otherwise>
	   					</c:choose>
	   						
	  					<c:forEach var="author" items="${authors}" varStatus="status"> 
	  						<c:if test="${author.id != newsWorkItem.author.id}">
		  						<option value="${ author.id }" >
		  							<c:out value="${ author.name }" />
		  						</option>	
	  						</c:if>
	  					</c:forEach>
					</select>
						
		  			<nav id="ulDropdown" class="ulDropdown createUlDropdown">
						<li id="ulDrop" class="first-child-li-dropdown">
							<c:choose>
								<c:when test="${empty newsWorkItem}">
									<span id="ulDropdownText"><spring:message code="message.tag.select" /></span>
								</c:when>
								<c:otherwise>
									<c:forEach var="tag" items="${newsWorkItem.tags}" varStatus="loop">  
								 		<span id="${tag}"><c:out value="${tag.tagName}"/><c:if test="${!loop.last}">,</c:if></span>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</li>
						<c:forEach var="tag" items="${tags}" varStatus="status">  
							<li class="dropdownTags"><input class="checkboxes-create-news" type="checkbox" value="${ tag.id }" name="tagId"/><span><c:out value="${ tag.tagName }" /></span></li>
		  				</c:forEach>
	  				</nav>
  				</section>
				<span id="selectTags"><spring:message code="message.news.create.isNull.tags" /></span>
				<section class="news-info-creation">
					<c:choose>	
					<c:when test="${empty newsWorkItem}">	 	
				 		  	<label for="title" ><spring:message code="message.news.title" />:</label>
		  					<textarea  name="title" class="inputTitle"></textarea>
		  					<span id="titleIsNull"><spring:message code="message.news.create.isNull.tittle" /></span>
		  					<span id="titleIsLarge"><spring:message code="message.news.create.isLarge.tittle" /></span>
		  			 			
		  			  		<label for="date" ><spring:message code="message.news.creationDate" />:</label>
						
							<fmt:formatDate  value="${creationDate}"
                				type="date" 
                				pattern="yyyy-MM-dd"
                				var="theFormattedDate" />
							<input type="date" name="date" value='${theFormattedDate}' required />		
		  					
		  					<label for="shortText" ><spring:message code="message.news.shortText" />:</label>
							<textarea   name="shortText" class="inputShortText"></textarea>
							<span id="shortTextIsNull"><spring:message code="message.news.create.isNull.shortText" /> </span>
		  					<span id="shortTextIsLarge"><spring:message code="message.news.create.isLarge.shortText" /></span>
							
							<label for="fullText" ><spring:message code="message.news.fullText" />:</label>
							<textarea name="fullText" class="inputFullText"></textarea>
		  					<span id="fullTextIsNull"><spring:message code="message.news.create.isNull.fullText" /></span>
		  					<span id="fullTextIsLarge"><spring:message code="message.news.create.isLarge.fullText" /></span>
							<input type="submit" class="create-news-button" value="<spring:message code="button.save" />" />
					</c:when>
					<c:otherwise>
							<label for="title" ><spring:message code="message.news.title" />:</label>
							<textarea  name="title" class="inputTitle"><c:out value="${ newsWorkItem.news.title }" /></textarea>
							<span id="titleIsNull"><spring:message code="message.news.create.isNull.tittle" /></span>
		  					<span id="titleIsLarge"><spring:message code="message.news.create.isLarge.tittle" /></span>	
					
							<label for="date" ><spring:message code="message.news.modificationDate" />:</label>
							<c:if test="${not empty newsWorkItem.news.modificationDate}">
								<fmt:formatDate  value="${newsWorkItem.news.modificationDate}"
                				type="date" 
                				pattern="yyyy-MM-dd"
                				var="theFormattedDate" />
								<input type="date" name="date" value='${theFormattedDate}' required />			
							</c:if>
							<c:if test="${empty newsWorkItem.news.modificationDate}">
								<input type="date" name="date"  required />			
							</c:if>
							
							<label for="shortText" ><spring:message code="message.news.shortText" />:</label>
							<textarea   name="shortText" class="inputShortText"><c:out value="${ newsWorkItem.news.shortText }" /></textarea>
							<span id="shortTextIsNull"><spring:message code="message.news.create.isNull.shortText" /> </span>
		  					<span id="shortTextIsLarge"><spring:message code="message.news.create.isLarge.shortText" /></span>
							
							<label for="fullText" ><spring:message code="message.news.fullText" />:</label>
							<textarea name="fullText" class="inputFullText"><c:out value="${ newsWorkItem.news.fullText }" /></textarea>
		  					<span id="fullTextIsNull"><spring:message code="message.news.create.isNull.fullText" /></span>
		  					<span id="fullTextIsLarge"><spring:message code="message.news.create.isLarge.fullText" /></span>
							<input type="submit" class="create-news-button" value="<spring:message code="button.save" />" />
					</c:otherwise>
					</c:choose>
				</section>
			</form>
		
			
	
		</div>
   		
