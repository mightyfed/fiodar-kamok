<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


		
		
	
		<div>
			<ul class="listTags">
			
				<c:forEach var="tag" items="${ tags }">
					<li>
						<b><spring:message code="label.tag" />:</b>
						
						<form method="POST" action="tags/update" id="update-author-tag">
							<input type="hidden" name="tagId" value="${ tag.id }" />
							<input type="text" name=tagName value="${ tag.tagName }" value="<c:out value="${ tag.tagName }"/>" id="tag<c:out value="${ tag.id }"/>" class="updateInput"  required readonly/>
							<button  type="button" class = "editTag" id="edit<c:out value="${ tag.id }"/>" value="${ tag.id }"> <spring:message code="button.edit" /></button>
							<button  type="button" class = "cancel" id="cancel<c:out value="${ tag.id }"/>" value="${ tag.id }"> <spring:message code="button.cancel" /></button>
							<input type="hidden" value="<spring:message code='button.update' />" id="updateButton<c:out value="${ tag.id }"/>" class="updateTagButton"/>
						</form>
						
					
						<form method="POST" action="tags/delete">
							<input type="hidden" name="tagId" value="${ tag.id }" />
							<input type="hidden" value="<spring:message code='button.delete' />" id="deleteButton<c:out value="${ tag.id }"/>" class="deleteTagButton"/>
						</form>
						
					</li>
				</c:forEach>
					<li>
						<form method="POST" action="tags/create" id="create-author-tag">
							<b><spring:message code="message.add.tag"/></b>
							<input type="text" name="tagName" id="tagName"/>
							<input type="submit" value=<spring:message code="button.save" /> class="save-author-button"/>
						</form>
					</li>
						<span id="tagIsNull"><spring:message code="message.news.create.isNull.tag" /></span>
		  				<span id="tagIsLarge"><spring:message code="message.news.create.isLarge.tag" /></span>
		  				<c:if test="${not empty tagExists}">
		  					<spring:message code="${tagExists}" />
						</c:if>		  				
				</ul>
			</div>
