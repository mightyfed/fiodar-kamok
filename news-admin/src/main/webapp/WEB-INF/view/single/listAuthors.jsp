<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


		<div>
			<ul class="listAuthors">
				<c:forEach var="author" items="${ authors }">
					<li>
						<b><spring:message code="label.author" />:</b>
						
						<form method="POST" action="authors/update">
							<input type="hidden" name="authorId" value="${ author.id }" />
							<input type="text" name=authorName value="${ author.name }" value="<c:out value="${ author.name }"/>" id="author<c:out value="${ author.id }"/>" class="updateInput" required readonly/>
							<button  type="button" class = "editAuthor" id="edit<c:out value="${ author.id }"/>" value="${ author.id }"> <spring:message code="button.edit" /></button>
							<button  type="button" class = "cancel" id="cancel<c:out value="${ author.id }"/>" value="${ author.id }"> <spring:message code="button.cancel" /></button>
							<input type="hidden" value="<spring:message code="button.update" />" id="updateButton<c:out value="${ author.id }"/>" class="updateAuthorButton"/>
						</form>
						
						<form method="POST" action="authors/delete">
							<input type="hidden" name="authorId" value="${ author.id }" />
							<input type="hidden" value="<spring:message code="button.expire" />" id="expiredButton<c:out value="${ author.id }"/>" class="expiredAuthorButton"/>
						</form>
						
					</li>
				</c:forEach>
					<li>
						<form method="POST" action="authors/create" id="create-author-tag">
							<b><spring:message code="message.add.author" /></b>
							<input type="text" name="authorName" id="tagName"/>
							<input type="submit" value="<spring:message code="button.save" />" class="save-author-button"/>
						</form>
					</li>
						<span id="tagIsNull"><spring:message code="message.news.create.isNull.author" /></span>
		  				<span id="tagIsLarge"><spring:message code="message.news.create.isLarge.author" /></span>
						
			</ul>
		</div>
	