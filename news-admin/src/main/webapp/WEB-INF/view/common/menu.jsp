<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<ul class="menu">
	<li><a href="${contextPath}/news-admin/news/page1"><spring:message code="menu.newsList" /></a></li>
	<li><a href="${contextPath}/news-admin/news/add/0"><spring:message code="menu.addNews" /></a></li>
	<li><a href="${contextPath}/news-admin/authors"><spring:message code="menu.addAuthors" /></a></li>
	<li><a href="${contextPath}/news-admin/tags"><spring:message code="menu.addTags" /></a></li>
</ul>