$(function() {
	
	$('#delete-news-button').on('click', function(e) {
		var selected = [];
		$('.CheckBoxToToDeleteTags:checked').each(function() {
		    selected.push($(this).attr('name'));
		});
		
		if(selected.length == 0) {
			$('#nullNewsToDelete').show();
			$('#nullNewsToDelete').css('color', 'red');
			$('#nullNewsToDelete').css('margin-left', '300px');
			$('#nullNewsToDelete').css('position', 'absolute');
			$('#nullNewsToDelete').css('bottom', '60px');
			return false;
		} else {
			$('#nullNewsToDelete').hide();
		}
		
		
		$('header').fadeTo( "slow", 0.2)
		$('footer').fadeTo( "slow", 0.2)
		$('.menu').fadeTo( "slow", 0.2)
		$('.pages').fadeTo("slow", 0.2)
		$('.nesFindForm').fadeTo("slow", 0.2)
		$('.inp2').fadeTo("slow", 0.2)
		
		$('.content').css('pointer-events', 'none');
		$('#delete-news-true').css('pointer-events', 'auto');
		$('#delete-news-false').css('pointer-events', 'auto');
		$('#delete-news-true').css('cursor', 'pointer');
		$('#delete-news-false').css('cursor', 'pointer');
		
		$('.content').find('#delete-news-form').children(':not(#popUpDeleteNews)').fadeTo( "slow", 0.2, function() {
			
			$('#popUpDeleteNews').show();
			
		} );
		
		
	})
	
	
	
	$('#delete-news-true').on('click', function() {
		$('#popUpDeleteNews').fadeTo( "fast", 1.0 );
	})
	
	$('#delete-news-false').on('click', function(e) {
		e.preventDefault();
		$('#popUpDeleteNews').hide();
		$('.content').find('#delete-news-form').children(':not(#popUpDeleteNews)').fadeTo( "slow", 1)
		$('header').fadeTo( "slow", 1)
		$('footer').fadeTo( "slow", 1)
		$('.menu').fadeTo( "slow", 1)
		$('.pages').fadeTo("slow", 1)
		$('.nesFindForm').fadeTo("slow", 1)
		$('.inp2').fadeTo("slow", 1)
		$('.content').css('pointer-events', 'auto');
		$('.content').css('pointer-events', 'auto');
		$('.content').css('pointer-events', 'auto');
		e.preventDefault();
	})
	
})