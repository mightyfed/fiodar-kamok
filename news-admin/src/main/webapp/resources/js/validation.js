$(function() {
	$('#titleIsLarge').hide();
	$('#titleIsNull').hide();
	$('#shortTextIsLarge').hide();
	$('#shortTextIsNull').hide();
	$('#fullTextIsLarge').hide();
	$('#fullTextIsNull').hide();
	$('#selectTags').hide();
	$('textarea').css("border", "1px solid gray");
	$("#ulDropdown").css("border", "1px solid gray");
	
	$('.newsCreation').on("submit", function(e) {
		validate(e);
	})
	
	
	$("input#tagName").css("border", "1px solid gray");
	$('#tagIsNull').hide();
	$('#tagIsLarge').hide();
	$('#create-author-tag').on("submit", function(e) {
		validateAuthorTag(e)
	})
	
	$('#update-author-tag').on("submit", function(e) {
		validateAuthorTag(e)
	})
	
})

function validate(e) {
			$('#titleIsLarge').hide();
			$('#titleIsNull').hide();
			$('#shortTextIsLarge').hide();
			$('#shortTextIsNull').hide();
			$('#fullTextIsLarge').hide();
			$('#fullTextIsNull').hide();
			$('#selectTags').hide();
			$('textarea').css("border", "1px solid gray");
			$("#ulDropdown").css("border", "1px solid gray");
			
			var title = $(".inputTitle").val();
			var shortText = $(".inputShortText").val();
			var fullText = $(".inputFullText").val();
			
			var checkBoxes = $(".checkboxes-create-news")
			var isChecked = false;
			for(i = 0; i <  checkBoxes.length; i++) { 
				if(checkBoxes[i].checked) {
					isChecked = true;
				}
			}
			
			if(isChecked === false) {
				$("#ulDropdown").css("border", "3px solid red");
				$('#selectTags').show();
				e.preventDefault();
				return;
				
			}
			
			if(title) {
				if(title.length > 30) {
					$('#titleIsLarge').show();
					$('.inputTitle').css("border", "1px solid red");
					e.preventDefault();
				}
				else {
					if(shortText) {
						if(shortText.length > 100) {
							$('#shortTextIsLarge').show();
							$('.inputShortText').css("border", "1px solid red");
							e.preventDefault();
						} else {
							if(fullText) {
								if(fullText.length > 2000) {
									$('#fullTextIsLarge').show();
									$('.inputFullText').css("border", "1px solid red");
									e.preventDefault();
								}
							} else {
								$('#fullTextIsNull').show();
								$('.inputFullText').css("border", "1px solid red");
								e.preventDefault();
							}
						}
					} else {
						$('#shortTextIsNull').show();
						$('.inputShortText').css("border", "1px solid red");
						e.preventDefault();
					}
	
					
				}
			} else {
				$('#titleIsNull').show();
				$('.inputTitle').css("border", "1px solid red");
				e.preventDefault();
			}
}


function validateAuthorTag(e) {
	$("input#tagName").css("border", "1px solid gray");
	$('#tagIsNull').hide();
	$('#tagIsLarge').hide();
	var name = $('#create-author-tag input#tagName').val();;
	if(name) {
		if(name.length > 30) {
			$('#tagIsLarge').show();
			$('input#tagName').css("border", "1px solid red");
			e.preventDefault();
		}
	} else {
		$('#tagIsNull').show();
		$('input#tagName').css("border", "1px solid red");
		e.preventDefault();
	}
}