package com.epam.newsmanagment.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;


/**
 * The {@code AuthorController} class is for binding views with services and (project logic).
 * It creates, expires, updates and gets list of authors.
 * This class provides four methods {@code getAuthorList}, {@code createAuthor}, {@code updateAuthor}, {@code expireAuthor}
 * Uses {@code AuthorService} value for communication with servers, database.
 * 
 * @author Fiodar_Kamok
 */
@Controller
@RequestMapping("/authors")
public class AuthorController {
	private final static Logger logger = Logger.getLogger(AuthorController.class);
	
	@Autowired 
	IAuthorService authorService;
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "")
	public String getAuthorList(ModelMap model) throws ServiceException {
		List<Author> authors = authorService.findAllAuthorsNoExpired();
		model.addAttribute("authors", authors);
		
		return "listAuthors";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public String createAuthor(@RequestParam("authorName") String authorName) throws ServiceException {
		
		Author author = new Author();
		author.setName(authorName);		
		authorService.addAuthor(author);
		
		return "redirect:/authors";	
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/update", method = RequestMethod.POST)
	public String updateAuthor(@RequestParam("authorId") Long authorId,
					@RequestParam("authorName") String authorName) throws ServiceException {
	
		Author editedAuthor = new Author();
		editedAuthor.setId(authorId);
		editedAuthor.setName(authorName);
		authorService.editAuthor(editedAuthor);
		
		return "redirect:/authors";	
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public String expireAuthor(@RequestParam("authorId") Long authorId) throws ServiceException {
		
		authorService.expireAuthor(authorId);
		
		return "redirect:/authors";	
	}
	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleCustomException(ServiceException e) { 
		logger.error(e);
		ModelAndView model = new ModelAndView("error/errorPage");
		model.addObject("errMsg", e.getMessage());
		model.addObject("errCause", e.getCause());
		return model;
 
	}
	
	
}
