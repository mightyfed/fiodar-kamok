package com.epam.newsmanagment.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ITagService;


/**
* The {@code TagController} class is for binding views with service and (project logic).
* It creates, deletes, updates and gets list of tags.
* This class provides four methods {@code getTagList}, {@code createTag}, {@code updateTag}, {@code deleteTag}
* Uses {@code TagService} value for communication with service, database.
* 
* @author Fiodar_Kamok
*/
@Controller
@RequestMapping("/tags")
public class TagController {
	private final static Logger logger = Logger.getLogger(TagController.class);
	
	@Autowired
	ITagService tagService;

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "")
	public String getTagList(ModelMap model) throws ServiceException {
		
		List<Tag> tags = tagService.findAllTags();
		model.addAttribute("tags", tags);
		
		return  "listTags";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/update", method = RequestMethod.POST) 
	public String updateTag(@RequestParam("tagId") Long tagId,
							@RequestParam("tagName") String tagName) throws ServiceException {
	
		Tag editedTag = new Tag();
		editedTag.setId(tagId);
		editedTag.setTagName(tagName);
		tagService.editTag(editedTag);
		
		return  "redirect:/tags";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public String deleteTag(@RequestParam("tagId") Long tagId) throws ServiceException {
		
		tagService.deleteTag(tagId);
		
		return  "redirect:/tags";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public String createTag(@RequestParam("tagName") String tagName) throws ServiceException {
	
		Tag tag = new Tag();
		tag.setTagName(tagName);
		tagService.addTag(tag);
		
		return  "redirect:/tags";
	}
	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleCustomException(ServiceException e) { 
		logger.error(e);
		ModelAndView model = new ModelAndView("error/errorPage");
		model.addObject("errMsg", e.getMessage());
		model.addObject("errCause", e.getCause());
		return model;
 
	}
}
