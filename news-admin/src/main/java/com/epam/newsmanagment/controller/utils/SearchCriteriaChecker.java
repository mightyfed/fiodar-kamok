package com.epam.newsmanagment.controller.utils;

import java.util.List;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.entity.SearchCriteria;


public class SearchCriteriaChecker {
	
	/**
	 * if pageId != previous pageId (previous pageId we get from session), than we get searchCriteria 
	 * from session, (when we move from one page to another, method takes null tagsId and null authorId)
	 * 		if searchCriteria from session is null, we get new SearchCriteria without search parameters
	 * if pageId == previous pageId (it means we change search parameters ), tagsId or authorId are not null, 
	 * we get new searchCriteria with new search parameters.
	 */
	public static SearchCriteria getSearchCriteria(List<Long> tagsId, Long authorId, Integer pageId, HttpSession session) {
		SearchCriteria searchCriteria = null;
		if((Integer)session.getAttribute("pageId") != null && pageId != (Integer)session.getAttribute("pageId")) {
			searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
			if(searchCriteria == null) {
				searchCriteria = new SearchCriteria();
			}
		} else {
			searchCriteria = new SearchCriteria(tagsId, authorId);
		}
		
		return searchCriteria;
	}
	
	/**
	 * if on some page we get new searchCriteria, we should redirect to first page 
	 */
	public static boolean isNewSearchCriteria(SearchCriteria searchCriteria, HttpSession session) {
		if( !searchCriteria.equals(session.getAttribute("searchCriteria"))) {
			session.setAttribute("searchCriteria", searchCriteria);
			return true;
		}
		return false;
	}
	
}
