package com.epam.newsmanagment.controller;

import java.util.Date;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ICommentService;




/**
 * The {@code CommentController} class is for binding views with service (project logic).
 * It creates and deletes comments.
 * This class provides two methods {@code createComment} and {@code deleteComment}
 * Uses {@code CommentService} value for communication with service, database.
 * 
 * @author Fiodar_Kamok
 */
@Controller
@RequestMapping("/news")
public class CommentController {

	private final static Logger logger = Logger.getLogger(CommentController.class);
	
	@Autowired
	ICommentService comementService;
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="post/{newsId}", method = RequestMethod.POST)
	public @ResponseBody Comment createComment(@PathVariable(value = "newsId") Long newsId, 
							@RequestParam(value = "commentText") String commentText, HttpSession session) throws ServiceException {
	
		Comment comment = new Comment();
		comment.setText(commentText);
		comment.setNewsId(newsId);
		comment.setCreationDate(new Date());
		
		Long id = comementService.addComment(comment);
		comment.setId(id);
		
		return comment;
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/post/{newsId}/delete" /*, method = RequestMethod.POST*/)
	public String deleteComment(@PathVariable(value = "newsId") Long newsId,
							@RequestParam("commentId") Long commentId, ModelMap model) throws ServiceException {
		
		comementService.deleteComment(commentId);
		
		return "redirect:/news/post/" + newsId;	
	} 
	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleCustomException(ServiceException e) { 
		logger.error(e);
		ModelAndView model = new ModelAndView("error/errorPage");
		model.addObject("errMsg", e.getMessage());
		model.addObject("errCause", e.getCause());
		return model;
	}
}
