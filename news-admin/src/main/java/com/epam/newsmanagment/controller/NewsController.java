package com.epam.newsmanagment.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.service.INewsManagerService;
import com.epam.newsmanagment.service.impl.NewsManagerServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.controller.utils.NeighborNewsFinder;
import com.epam.newsmanagment.controller.utils.PageCounter;
import com.epam.newsmanagment.controller.utils.SearchCriteriaChecker;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.NewsVO;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.ServiceException;

/**
* The {@code NewsController} class is for binding views with service and (project logic).
* It creates, deletes, updates and gets list of news.
* 
* Uses {@code NewsManagementService} value for communication with service, database.
* 
* @author Fiodar_Kamok
*/
@Controller
@RequestMapping("/news")
public class NewsController {
	private final static Logger logger = Logger.getLogger(NewsController.class);
	
	@Value("${NUM_NEWS_ON_PAGE}")
	private int NUM_NEWS_ON_PAGE;
	private SearchCriteria searchCriteria;
	
	@Autowired
	INewsManagerService newsManageService;
	
	/**
	 * returns list of news according to {@code SearchCriteria} object, and page index
	 * Also creates {@code List<Long> newsIds} for transition on next or previous news
	 * @throws ServiceException 
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/page{pageId}")
	public String getNewsList(@PathVariable(value = "pageId") Integer pageId,
								@RequestParam(value="authorId", required=false) Long authorId, 
								@RequestParam(value="tagId", required=false) List<Long> tagsId,
								ModelMap model, HttpSession session ) throws ServiceException {
		
		List<NewsVO> newsWorkItemList = new ArrayList<NewsVO>();
		List<Author> authors = new ArrayList<Author>();
		List<Tag> tags = new ArrayList<Tag>();
		List<Long> newsIds = new ArrayList<Long>();
		String result = null;
		int numOfPages = 0;
		
		/*
		 * if pageId != previous pageId (previous pageId we get from session), than we get searchCriteria 
		 * from session, (when we move from one page to another, method takes null tagsId and null authorId)
		 * 		if searchCriteria from session is null, we get new SearchCriteria without search parameters
		 * if pageId == previous pageId (it means we change search parameters ), tagsId or authorId are not null, 
		 * we get new searchCriteria with new search parameters.
		 */
		searchCriteria = SearchCriteriaChecker.getSearchCriteria(tagsId, authorId, pageId, session);
		
		// if on some page we get new searchCriteria, we should move to first page 
		if(SearchCriteriaChecker.isNewSearchCriteria(searchCriteria, session) && pageId != 1) {
			result = "redirect:/news/page1";
		}
	if (result == null) {
		newsWorkItemList = newsManageService.findNewsVOList(searchCriteria, pageId, NUM_NEWS_ON_PAGE);

		// fill List<Long> newsIds with news ids from current page
		for (NewsVO newsVO : newsWorkItemList) {
			News news = newsVO.getNews();
			newsIds.add(news.getId());
		}

		authors = newsManageService.getAuthorService().findAllAuthorsNoExpired();
		tags = newsManageService.getTagService().findAllTags();

		numOfPages = PageCounter.countPageNumber(searchCriteria, newsManageService, NUM_NEWS_ON_PAGE);

		if (numOfPages == 0) {
			model.addAttribute("newsNotFound", "message.newsNotFound");
		}

		model.addAttribute("newsList", newsWorkItemList);
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
		model.addAttribute("numOfPages", numOfPages);
		session.setAttribute("searchCriteria", searchCriteria);
		session.setAttribute("newsIds", newsIds);
		session.setAttribute("pageId", pageId);

		result = "listNews";
	}
		return result ;
	}
	
	
	/**
	 * resets search criteria parameters, and sets page index to 1;
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/reset", method = RequestMethod.POST)
	public String reset(HttpSession session ) {
		
		session.setAttribute("searchCriteria", null);
		session.setAttribute("pageId", 1);
		return "redirect:/news/page1";
	}
	
	
	/**
	 * gets news, and finds next news id, and previous news id, if they exist.
	 * Finds {@code nextNewsId} and {@code previousNewsId} in {@code List<Long> newsIds}. 
	 * If necessary complements {@code List<Long> newsIds} with {@code newsId} from next or previous page
	 * @throws ServiceException 
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="post/{newsId}", method = RequestMethod.GET)
	public String getNews(@PathVariable("newsId") Long newsId, ModelMap model, HttpSession session) throws ServiceException {
		
		NewsVO news = newsManageService.findNewsVOById(newsId);
	
		Integer pageId = (Integer) session.getAttribute("pageId");
		
		/*
		 * List<Long> newsIds - list of news id's filtered by searchCriteria 
		 * and ordered by comments number and modification date
		*/
		List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");
		
		/*
		 * We find index of current news in this list, and get next(previous) index.
		 * We use two methods to find next(previous) index: 
		 * 		NeighborNewsFinder.findNextNewsId(),  NeighborNewsFinder.findPreviousNewsId()
		 * if we haven't in this list next(previous) index, 
		 * 		1) we check for page neighbors(next or previous page) 
 		 * 		if we can get next(previous) page
 		 * 			2) we finds news id's from next (previous) page, and add them to list
 		 * 		else 
 		 * 			2) we does not find next(previous) news id's, they are null
		 */		
		if(pageId != null && newsIds != null) {
			int currentIndex = newsIds.indexOf(newsId);
			pageId = NeighborNewsFinder.checkForPageNeighbors(currentIndex, NUM_NEWS_ON_PAGE, pageId, newsIds);
		}
		
		session.setAttribute("pageId", pageId);
		session.setAttribute("currNewsId", news.getNews().getId());	
		model.addAttribute("newsWorkItem", news);
		model.addAttribute("nextNewsId", NeighborNewsFinder.findNextNewsId(newsId, session, newsManageService, NUM_NEWS_ON_PAGE));
		model.addAttribute("previousNewsId", NeighborNewsFinder.findPreviousNewsId(newsId, session, newsManageService, NUM_NEWS_ON_PAGE));
		
		return "singleNews";
	}
	
	
	/**
	 * deletes news list
	 * @throws ServiceException 
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public String deleteNews(@RequestParam(value="newsId", required=false) List<Long> newsIds, 
						@RequestParam(value="pageId") Integer pageId,
						ModelMap model) throws ServiceException {
		
		newsManageService.deleteNews(newsIds);
		
		return "redirect:/news/page" + pageId;
	}
	
	
	/**
	 * 
	 * forwards to create news page, or to edit news page.
	 * It depends on url. 
	 * url:  /add/0  forwards to create page,
	 * url:  /add/{@code newsId} newsId is not zero forwards to edit page
	 * @throws ServiceException 
 	 * 
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/add/{newsId:^\\d*$}", method = RequestMethod.GET)
	public String addNewsView (@PathVariable(value = "newsId") Long newsId,	ModelMap model ) throws ServiceException {
		
		List<Author> authors = newsManageService.getAuthorService().findAllAuthorsNoExpired();
		List<Tag> tags = newsManageService.getTagService().findAllTags();
		
		// if newsId != null, than finds news by id, for edit news
		// else sets current date to model, for creation news
		if(newsId != 0) {		
			NewsVO newsWorkItem = newsManageService.findNewsVOById(newsId);
			model.addAttribute("newsWorkItem", newsWorkItem);				
		} else {
			model.addAttribute("creationDate", new Date());		
		}
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
		model.addAttribute("newsId", newsId);		
		
		return "createNews";
	}
	

	/**
	 * 	creates or edits news. It depends on url.
	 *	if  url:  /add/0 then creates news,
	 * 	else if  url:  /add/{@code newsId} and newsId is not zero, then edits news
	 * @throws ServiceException 
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/add/{newsId}", method = RequestMethod.POST) 
	public String saveNews(@PathVariable(value = "newsId") Long newsId,
				@RequestParam(value="title") String title, 
				@RequestParam(value="date", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date creationDate,  
				@RequestParam(value="date", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date modificationDate,  
				@RequestParam(value="shortText") String shortText,
				@RequestParam(value="fullText") String fullText, 
				@RequestParam(value="tagId") List<Long> tagsId, 
				@RequestParam(value="authorId") Long authorId, 
				ModelMap model, HttpSession session ) throws ServiceException {
		
		News news = new News();
		news.setId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		
		// if news id = 0, than sets creation date and same modification date, for creation news
		// else sets only modification date
		if(newsId == 0) {
			news.setCreationDate(creationDate);
			news.setModificationDate(creationDate);
		} else {
			news.setModificationDate(modificationDate);
		}		
		newsId = newsManageService.saveNews(news, authorId, tagsId);
		
		return "redirect:/news/post/" + newsId;
	}
	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleCustomException(ServiceException e) { 
		logger.error(e);
		ModelAndView model = new ModelAndView("error/errorPage");
		model.addObject("errMsg", e.getMessage());
		model.addObject("errCause", e.getCause());
		return model;
	}
	
	
	
	
}
