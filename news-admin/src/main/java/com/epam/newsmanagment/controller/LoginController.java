package com.epam.newsmanagment.controller;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
* The {@code LoginController} class is for login user.
* 
* @author Fiodar_Kamok
*/
@Controller
public class LoginController {

	@Secured("ROLE_ANONYMOUS")
	@RequestMapping(value="/login", method = RequestMethod.GET)
    public String loginView(Model model)throws AccessDeniedException {
        return "login";
    }
	
	@Secured("ROLE_ANONYMOUS")
	@RequestMapping(value="/loginError", method = RequestMethod.GET)
    public String loginViewError(Model model) throws AccessDeniedException {
		model.addAttribute("loginError", "message.user.notEsists");
        return "login";
    }
	
	@ExceptionHandler(AccessDeniedException.class)
	public String handleCustomException(AccessDeniedException e) { 
		return "redirect:/news/page1";
	}
	
}
