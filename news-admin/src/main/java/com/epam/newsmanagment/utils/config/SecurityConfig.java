package com.epam.newsmanagment.utils.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.epam.newsmanagment.utils.service.UserDetailsServiceImpl;



@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private UserDetailsServiceImpl userDetailsService;
 
    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(getShaPasswordEncoder());
    }

	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	      
	     //  CSRF 
	        http.csrf()
	                .disable()
	                // указываем правила запросов
	                // по которым будет определятся доступ к ресурсам и остальным данным
	                .authorizeRequests()
	                .antMatchers("/resources/**", "/**").permitAll()
	                .anyRequest().permitAll()
	                .and();
	 
	        http.formLogin()
	                // page with login form
	                .loginPage("/login")
	                .loginProcessingUrl("/j_spring_security_check")
	                // url when login fail
	                .failureUrl("/loginError")
	                // username and password
	                .usernameParameter("j_username")
	                .passwordParameter("j_password")
	                .permitAll();
	 
	        http.logout()
	                .permitAll()
	                // logout url
	                .logoutUrl("/logout")
	                // url when logout success
	                .logoutSuccessUrl("/login")
	                .invalidateHttpSession(true);      
	    }
	    
	    @Bean
		 public ShaPasswordEncoder getShaPasswordEncoder(){
		      return new ShaPasswordEncoder();
		 }
}
