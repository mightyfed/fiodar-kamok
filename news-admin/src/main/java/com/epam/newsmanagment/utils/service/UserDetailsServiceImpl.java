package com.epam.newsmanagment.utils.service;

import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.epam.newsmanagment.entity.User;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IUserService;




public class UserDetailsServiceImpl implements UserDetailsService{
	private final static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class);
	
	@Autowired
	private IUserService userService;
	
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = null;
		try {
			user = userService.findUserByLogin(login);
		} catch (ServiceException e) {
			logger.error(e);
			throw new RuntimeException();
		}
		Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority(user.getRole()));
		UserDetails userDetails = 
				new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), roles);
		return userDetails;
	}
	
	 

}
