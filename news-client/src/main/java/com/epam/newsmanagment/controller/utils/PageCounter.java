package com.epam.newsmanagment.controller.utils;

import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.INewsManagerService;
import com.epam.newsmanagment.service.impl.NewsManagerServiceImpl;

public class PageCounter {
	
	public static int countPageNumber(SearchCriteria searchCriteria, INewsManagerService newsManageService, int numNewsOnPage) throws ServiceException {
		int numNews = newsManageService.getNewsService().countNumNews(searchCriteria);
		int numOfPages = numNews / numNewsOnPage;
		if(numNews % numNewsOnPage != 0) {
			numOfPages += 1;
		} 
		return numOfPages;
	}
}
