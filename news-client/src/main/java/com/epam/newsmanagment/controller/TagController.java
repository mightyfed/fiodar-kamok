package com.epam.newsmanagment.controller;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


/**
* The {@code TagController} class is for binding views with service and (project logic).
* It gets list of tags.
* This class provides four methods {@code getTagList}
* Uses {@code TagService} value for communication with service, database.
* 
* @author Fiodar_Kamok
*/

@Controller
@RequestMapping("/tags")
public class TagController {
	private final static Logger logger = Logger.getLogger(TagController.class);
	
	@Autowired
	ITagService tagService;

	@RequestMapping(value = "")
	public String getTagList(ModelMap model) throws ServiceException {
		
		List<Tag> tags = tagService.findAllTags();
		model.addAttribute("tags", tags);
		
		return  "listTags";
	}
	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleCustomException(ServiceException e) { 
		logger.error(e);
		ModelAndView model = new ModelAndView("error/errorPage");
		model.addObject("errMsg", e.getMessage());
		model.addObject("errCause", e.getCause());
		return model;
 
	}
}
