package com.epam.newsmanagment.controller;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ICommentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import java.util.Date;


/**
 * The {@code CommentController} class is for binding views with service (project logic).
 * It creates comments.
 * This class provides method {@code createComment}
 * Uses {@code CommentService} value for communication with service, database.
 * 
 * @author Fiodar_Kamok
 */

@Controller
@RequestMapping("/news")
public class CommentController {

	private final static Logger logger = Logger.getLogger(CommentController.class);
	
	@Autowired
	ICommentService comementService;
	

	@RequestMapping(value="post/{newsId}", method = RequestMethod.POST)
	public @ResponseBody Comment createComment(@PathVariable(value = "newsId") Long newsId, 
							@RequestParam(value = "commentText") String commentText, HttpSession session) throws ServiceException {
	
		Comment comment = new Comment();
		comment.setText(commentText);
		comment.setNewsId(newsId);
		comment.setCreationDate(new Date());
		
		Long id = comementService.addComment(comment);
		comment.setId(id);
		
		return comment;
	}

}
