package com.epam.newsmanagment.controller.utils;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.service.INewsManagerService;
import com.epam.newsmanagment.service.impl.NewsManagerServiceImpl;
import org.apache.log4j.Logger;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;


public class NeighborNewsFinder {
	private final static Logger logger = Logger.getLogger(NeighborNewsFinder.class);
	private static boolean isPossibleGetPreviousPage = false;
	private static boolean isPossibleGetNextPage = false;
	
	/**
	 * @param currNewsId - index of current news in List<Long> newsIds
	 * finds next news id for current news
	 * @throws ServiceException 
	 *
	 */
	public static Long findNextNewsId(Long currNewsId, HttpSession session,
									INewsManagerService newsManageService, int numNewsOnPage) throws ServiceException {
		
		List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");
		Integer pageId = (Integer) session.getAttribute("pageId");
		
		if(pageId == null || newsIds == null) {
			return null;
		}
		
		SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
		int numOfPages = PageCounter.countPageNumber(searchCriteria, newsManageService, numNewsOnPage);
		
		int currIndex = newsIds.indexOf(currNewsId);
		Long nextNewsIndex = null;
	
		/*
		 *  if we have next news id in List<Long> newsIds, we get it 
		 *  else  we finds news ids from next page and add them all to List<Long> newsIds,
		 *  than we get next news id
		 */
		if(currIndex+1 < newsIds.size()) {
			nextNewsIndex = newsIds.get(currIndex+1);
		} 
		else if (currIndex+1 == newsIds.size() && pageId != numOfPages) { 
			List<News> news = newsManageService.getNewsService().findByFilters(searchCriteria, pageId + 1, numNewsOnPage);
			for(int i = 0; i < news.size(); i++) {
				newsIds.add(news.get(i).getId());
			}
			nextNewsIndex = newsIds.get(currIndex+1);
		} 
		return nextNewsIndex;
	}
	
	/**
	 * @param currNewsId - index of current news in List<Long> newsIds
	 * finds previous news id for current news
	 * @throws ServiceException 
	 */
	public static Long findPreviousNewsId(Long currNewsId, HttpSession session,
										INewsManagerService newsManageService, int numNewsOnPage) throws ServiceException {
		
		List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");
		Integer pageId = (Integer) session.getAttribute("pageId");
		if(pageId == null || newsIds == null) {
			return null;
		}
		
		int currIndex = newsIds.indexOf(currNewsId);
		Long previousNewsIndex = null;
		
		/*
		 *  if we have previous news id in List<Long> newsIds, we get it 
		 *  else  we finds previous ids from previous page and add them all to List<Long> newsIds,
		 *  than we get previous news id
		 */
		if(currIndex-1 >= 0) {
			previousNewsIndex = newsIds.get(currIndex-1);
		}
		else if (currIndex-1 < 0 && pageId != 1) { 
			SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
				List<News> news = newsManageService.getNewsService().findByFilters(searchCriteria, pageId - 1, numNewsOnPage);
				for(int i = 0; i < news.size(); i++) {
					newsIds.add(i, news.get(i).getId());
				}
			currIndex += numNewsOnPage;
			previousNewsIndex = newsIds.get(currIndex-1);
		}
		return previousNewsIndex;
	}
	
	
	
/**
 * 	checks is it possible to get previous(next) page or this is first(last) page and it hasn't neighbors
 * 	also(if it is necessary) changes page index, when we move between pages by links: next or previous 
 *  @param newsIds - List<Long> newsIds - list of news id's filtered by searchCriteria 
 *  	and ordered by comments number and modification date
 *  @param currentIndex - index of current news in List<Long> newsIds
 *  @param pageId - id of current page
 *  @param numNewsOnPage - number news on page
 */
public static Integer checkForPageNeighbors(int currentIndex, int numNewsOnPage, Integer pageId, List<Long> newsIds) {
		
		if(currentIndex % numNewsOnPage == numNewsOnPage - 1) {
			isPossibleGetNextPage = true;
		}
		
		if(currentIndex % numNewsOnPage == 0 ) {
			isPossibleGetPreviousPage = true;
		} 
		
		if(newsIds.size() > numNewsOnPage && currentIndex % numNewsOnPage == 0 && isPossibleGetNextPage) {
			pageId++;
			isPossibleGetPreviousPage = true;
			isPossibleGetNextPage = false;
		}
		
		else if(newsIds.size() > numNewsOnPage && currentIndex % numNewsOnPage == numNewsOnPage - 1 && isPossibleGetPreviousPage) {
			pageId--;
			isPossibleGetNextPage = true;
			isPossibleGetPreviousPage = false;
		} 
		
		if(currentIndex % numNewsOnPage != numNewsOnPage - 1) {
			isPossibleGetNextPage = false;
		}
		
		if(currentIndex % numNewsOnPage != 0) {
			isPossibleGetPreviousPage = false;
		}
		return pageId;
	}
}
