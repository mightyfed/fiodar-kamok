package com.epam.newsmanagment.controller;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


/**
 * The {@code AuthorController} class is for binding views with services and (project logic).
 * It gets list of authors.
 * This class provides method {@code getAuthorList}
 * Uses {@code AuthorService} value for communication with servers, database.
 * 
 * @author Fiodar_Kamok
 */
@Controller
@RequestMapping("/authors")
public class AuthorController {
	private final static Logger logger = Logger.getLogger(AuthorController.class);
	
	@Autowired 
	IAuthorService authorService;


	@RequestMapping(value = "")
	public String getAuthorList(ModelMap model) throws ServiceException {
		List<Author> authors = authorService.findAllAuthorsNoExpired();
		model.addAttribute("authors", authors);
		
		return "listAuthors";
	}

	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleCustomException(ServiceException e) { 
		logger.error(e);
		ModelAndView model = new ModelAndView("error/errorPage");
		model.addObject("errMsg", e.getMessage());
		model.addObject("errCause", e.getCause());
		return model;
 
	}
	
	
}
