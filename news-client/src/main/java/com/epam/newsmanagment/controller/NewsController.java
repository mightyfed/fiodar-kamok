package com.epam.newsmanagment.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagment.service.INewsManagerService;
import com.epam.newsmanagment.service.impl.NewsManagerServiceImpl;
import com.sun.org.apache.bcel.internal.generic.INEG;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagment.controller.utils.NeighborNewsFinder;
import com.epam.newsmanagment.controller.utils.PageCounter;
import com.epam.newsmanagment.controller.utils.SearchCriteriaChecker;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.NewsVO;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.ServiceException;

/**
 * The {@code NewsController} class is for binding views with service and (project logic).
 * It gets list of news.
 * Uses {@code NewsManagementService} value for communication with service, database.
 * @author Fiodar_Kamok
 */
@Controller
@RequestMapping("/news")
public class NewsController {
	private final static Logger logger = Logger.getLogger(NewsController.class);

	@Value("${NUM_NEWS_ON_PAGE}")
	private int NUM_NEWS_ON_PAGE;
	private SearchCriteria searchCriteria;

	@Autowired
	INewsManagerService newsManageService;

	@RequestMapping(value="/page{pageId}")
	public String getNewsList(@PathVariable(value = "pageId") Integer pageId,
							  @RequestParam(value="authorId", required=false) Long authorId,
							  @RequestParam(value="tagId", required=false) List<Long> tagsId,
							  ModelMap model, HttpSession session ) throws ServiceException {

		List<NewsVO> newsWorkItemList = new ArrayList<NewsVO>();
		List<Author> authors = new ArrayList<Author>();
		List<Tag> tags = new ArrayList<Tag>();
		List<Long> newsIds = new ArrayList<Long>();
		int numOfPages = 0;

		searchCriteria = SearchCriteriaChecker.getSearchCriteria(tagsId, authorId, pageId, session);

		if(SearchCriteriaChecker.isNewSearchCriteria(searchCriteria, session) && pageId != 1) {
			return "redirect:/news/page1";
		}

		newsWorkItemList = newsManageService.findNewsVOList(searchCriteria, pageId, NUM_NEWS_ON_PAGE);

		for(NewsVO newsVO :  newsWorkItemList) {
			News news = newsVO.getNews();
			newsIds.add(news.getId());
		}

		authors = newsManageService.getAuthorService().findAllAuthorsNoExpired();
		tags = newsManageService.getTagService().findAllTags();

		numOfPages = PageCounter.countPageNumber(searchCriteria, newsManageService, NUM_NEWS_ON_PAGE);

		if( numOfPages == 0) {
			model.addAttribute("newsNotFound", "message.newsNotFound");
		}

		model.addAttribute("newsList", newsWorkItemList);
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
		model.addAttribute("numOfPages", numOfPages);
		session.setAttribute("searchCriteria", searchCriteria);
		session.setAttribute("newsIds", newsIds);
		session.setAttribute("pageId", pageId);

		return "listNews";
	}

	@RequestMapping(value="/reset", method = RequestMethod.POST)
	public String reset(HttpSession session ) {

		session.setAttribute("searchCriteria", null);
		session.setAttribute("pageId", 1);
		return "redirect:/news/page1";
	}

	@RequestMapping(value="post/{newsId}", method = RequestMethod.GET)
	public String getNews(@PathVariable("newsId") Long newsId, ModelMap model, HttpSession session) throws ServiceException {

		NewsVO news = newsManageService.findNewsVOById(newsId);

		Integer pageId = (Integer) session.getAttribute("pageId");

		List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");

		if(pageId != null && newsIds != null) {
			int currentIndex = newsIds.indexOf(newsId);
			pageId = NeighborNewsFinder.checkForPageNeighbors(currentIndex, NUM_NEWS_ON_PAGE, pageId, newsIds);
		}

		session.setAttribute("pageId", pageId);
		session.setAttribute("currNewsId", news.getNews().getId());
		model.addAttribute("newsWorkItem", news);
		model.addAttribute("nextNewsId", NeighborNewsFinder.findNextNewsId(newsId, session, newsManageService, NUM_NEWS_ON_PAGE));
		model.addAttribute("previousNewsId", NeighborNewsFinder.findPreviousNewsId(newsId, session, newsManageService, NUM_NEWS_ON_PAGE));

		return "singleNews";
	}

	@ExceptionHandler(ServiceException.class)
	public ModelAndView handleCustomException(ServiceException e) {
		logger.error(e);
		ModelAndView model = new ModelAndView("error/errorPage");
		model.addObject("errMsg", e.getMessage());
		model.addObject("errCause", e.getCause());
		return model;
	}




}
