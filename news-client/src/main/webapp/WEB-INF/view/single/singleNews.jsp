<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>	

<fmt:setLocale value="${pageContext.response.locale}" />
<spring:message code="pattern.date" var="pattern"/>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
	<script type="text/javascript" src="<c:url value='/resources/js/displayDate.js'/>"></script>
	
	<script type="text/javascript">




    	function postCommentAjax() {
    				var postData = $('.post-comment-form').serialize();
    				var commentText = $("#commentText").val();
    				if(commentText) {
    					if(commentText.length <= 100) {
    						$.ajax({
    							 type : 'POST',
    							 dataType: 'json',
    				             url : '${contextPath}/news-client/news/post/${ newsWorkItem.news.id }',
    				             data: postData,
    				              	success : function(data) {
    				              		$('#commentText').val('');
    				              		var date = new Date(data.creationDate)
    										"${pattern}"
    				                	$("#commentSection ul").prepend(
    				                		'<li>' +
    				    					 	'<span class="commenet-date">' + displayDate(date, "${pattern}") + '</span> ' +
    				    						'<span class="comment-text">' + data.text + '</span>' +
    											 +
    										'</li>' );
    				                },
    				       			error: function(e){
    				       		        alert('Error: ' + e);
    				       		    }
    						});
    					} else {
    						$('#commentIsLarge').show();
    						$('textarea').css("border", "1px solid red");
    					}
    				} else {
    					$('#commentIsNull').show();
    					$('textarea').css("border", "1px solid red");
    				}
    			}


    			$(function() {
    				$('.post-comment-form').on("submit", function(e) {
    					$('#commentIsLarge').hide();
    					$('#commentIsNull').hide();
    					$('textarea').css("border", "1px solid gray");
    					e.preventDefault();
    					postCommentAjax();
    				})

    			});
    		</script>

		
		<div>
			<a class="back" href="${contextPath}/news-client/news/page<c:out value="${ pageId }" />"><spring:message code="link.back"/></a>
				
				<section class="news-top"> 
					<h4>
						<c:out value="${ newsWorkItem.news.title }" />
					</h4>
					<span class="author">
						(<spring:message code="message.by"/> <c:out value="${ newsWorkItem.author.name }" />)
					</span>
							
					<span class="date">
						<fmt:formatDate  value="${newsWorkItem.news.modificationDate}"
			                		type="date" 
			                		pattern="${pattern}"
			                		var="theFormattedDate" />
                		${theFormattedDate}
					</span>
	  				
				</section>
				<section class="text">
					<p><c:out value="${ newsWorkItem.news.fullText }" /></p>
				</section>

			<section class="comment-section" id="commentSection">
				<ul>
					<c:forEach var="elem" items="${newsWorkItem.comments}" varStatus="status">
					<li id="${elem.id}">
					 	<span class="commenet-date">
					 		<fmt:formatDate  value="${elem.creationDate}"
                            			    type="date"
                            			    pattern="${pattern}"
                            			    var="theFormattedDate" />
                            	${theFormattedDate}
					 	</span>


						<span class="comment-text"><c:out value="${elem.text}"></c:out></span>

					</li>
					</c:forEach>
				</ul>
			</section>

			
			<span id="commentIsNull"><spring:message code="message.commentIsNull" /></span>
			<span id="commentIsLarge"><spring:message code="message.commentIsLarge" /></span>
			
	  		<form class="post-comment-form" action="${contextPath}/news-client/news/post/<c:out value='${ newsWorkItem.news.id }'/>">
				<textarea id="commentText" name="commentText"></textarea>
				<input type="submit" value="<spring:message code="button.post.comment"/>"/>
			</form>
			
			<c:if test="${not empty previousNewsId}" >
				<a class="previous" href="<c:out value="${ previousNewsId }" />"><spring:message code="link.previous"/></a>
			</c:if>
			<c:if test="${not empty nextNewsId}" >
   				<a class="next" href="<c:out value="${ nextNewsId }" />"><spring:message code="link.next"/></a>
   			</c:if>
			
		</div>
   		
   		
   		
   		
   	