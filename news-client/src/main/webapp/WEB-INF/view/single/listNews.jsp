<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${pageContext.response.locale}" />	
<spring:message code="pattern.date" var="pattern"/>	

		<div>
			
			 <form action="${contextPath}/news-client/news/page${pageId}" method="post" class="nesFindForm">
   					<select name="authorId">
   				  	
	   					<c:if test="${empty searchCriteria.authorId}">
	   						<option selected value=""><spring:message code="message.author.select" /> </option>
	   					</c:if>
   						<c:if test="${not empty searchCriteria.authorId}">
	   						<option  value=""><spring:message code="message.author.select" /> </option>
	   					</c:if>
   					
  						<c:forEach var="author" items="${authors}" varStatus="status"> 
	  						<c:choose>
	  							<c:when test="${author.id != searchCriteria.authorId}">
		  							<option value="${ author.id }" >
		  								<c:out value="${ author.name }" />
		  							</option>
	  							</c:when>
	  							<c:otherwise>
			   						<option selected value="${ author.id }" >
		  								<c:out value="${ author.name }" />
		  							</option>	
			   					</c:otherwise>
	  						</c:choose>
  						</c:forEach>
					</select>
					
					<nav id="ulDropdown" class="ulDropdown">
						<li id="ulDrop" class="first-child-li-dropdown">
							<c:choose>
								<c:when test="${empty searchCriteria.tagsId}">
									<span id="ulDropdownText"><spring:message code="message.tag.select" /></span>
								</c:when>
								<c:otherwise>
									<c:forEach var="searchTagId" items="${searchCriteria.tagsId}" varStatus="loop">  
										<c:forEach var="tag" items="${tags}" >
											<c:if test="${searchTagId == tag.id}"><span id="${ tag.tagName }"><c:out value="${tag.tagName}"/><c:if test="${!loop.last}">,</c:if></span></c:if>
										</c:forEach>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</li>
						<c:forEach var="tag" items="${tags}" varStatus="status">  
							<li class="dropdownTags"><input class="checkboxes-create-news" type="checkbox" value="${ tag.id }" name="tagId"/><span><c:out value="${ tag.tagName }" /></span></li>
		  				</c:forEach>
	  				</nav>
		
   					<p><input type="submit" name="submit" class="inp1" value="<spring:message code="button.filter" />" /></p>
   					
  			</form>
  			
  			<form action="${contextPath}/news-client/news/reset" method="post" class="resetForm">
  				<p><input type="submit" name="reset" class="inp2" value="<spring:message code="button.reset" />" /></p>
  			</form>
	
			<ul class="news-list">
				<form action="${contextPath}/news-client/news/delete" method="post" id="delete-news-form">
				<c:forEach var="elem" items="${newsList}" varStatus="status"> 
					<li>
						<article>
							<section class="news-top">
							<h4>
								<a href='${contextPath}/news-client/news/post/<c:out value="${elem.news.id}"/>' ><c:out value="${ elem.news.title }" /></a>
							</h4>
							<span class="author">
								(<spring:message code="message.by"/> <c:out value="${ elem.author.name }" />)
							</span>
							
							<span class="date">
								<c:choose>
									<c:when test="${not empty elem.news.modificationDate}">
										
										<fmt:formatDate  value="${elem.news.modificationDate}"
			                				type="date" 
			                				pattern="${pattern}"
			                				var="theFormattedDate" />
                						${theFormattedDate}
									</c:when>
									<c:otherwise>
										
										<fmt:formatDate  value="${elem.news.creationDate}"
			                				type="date" 
			                				pattern="${pattern}"
			                				var="theFormattedDate" />
									${theFormattedDate}
									</c:otherwise>
								</c:choose>
							</span>
							
							<p>
								<c:out value="${ elem.news.shortText }" />
							</p>
							</section>
				
							<section class="news-bottom">
								<ul class="tags">
									<c:forEach var="e" items="${elem.tags}" varStatus="loop"> 
										<li><c:out value="${ e.tagName }" /></li>
										<c:if test="${!loop.last}">,</c:if>
									</c:forEach>
								</ul>
								
								<span class="comments">
									<spring:message code="label.comments"/>(<c:out value="${ fn:length(elem.comments) }" />)
								</span>

							</section>
								
						</article>   
					</li>
				</c:forEach>
					<input type="hidden" name="pageId" value="${pageId}">
				</form>
			</ul>
			
			<c:if test="${not empty newsNotFound}">
				<span class="news-not-found"><spring:message code="${newsNotFound}" /></span>
			</c:if>
			

			
			<ul class="pages">
				<c:if test="${pageId-2>0}">
					<li><a href='/news-client/news/page<c:out value="${ pageId-2 } " />'>${pageId-2}</a></li>
				</c:if>
				<c:if test="${pageId-1>0}">
					<li><a href='/news-client/news/page<c:out value="${ pageId-1 } " />'>${pageId-1}</a></li>
				</c:if>
				<li class="current-page"><a href='/news-client/news/page<c:out value="${ pageId }" />'>${pageId}</a></li>
				<c:if test="${numOfPages >= pageId+1}">
					<li><a href='/news-client/news/page<c:out value="${ pageId+1 }" />'>${pageId+1}</a></li>
				</c:if>
				<c:if test="${numOfPages >= pageId+2}">
					<li><a href='/news-client/news/page<c:out value="${ pageId+2 }" />'>${pageId+2}</a></li>
				</c:if>
				 
				 
			<!--  	 <span class="page-number">${numOfPages}</span>-->
				
			</ul>
			 
			 
   		</div>
   		
   		
