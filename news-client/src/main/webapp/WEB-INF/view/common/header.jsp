<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>



	<header>
		<h2><spring:message code="label.newsPortal" /></h2>
		<section>
	
		<span>
			<a href="?language=en_EN"><spring:message code="language.EN"/></a>   <a href="?language=ru_RU"><spring:message code="language.RU"/></a>
		</span>
		</section>
		
		
	</header>
	