<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet">

<html>
<head>
	<title>404 Error Page</title>
</head>
<body>
 			<div class="error-container">
	           	<h2>Page Not Found.</h2>
			</div>
</body>
</html>