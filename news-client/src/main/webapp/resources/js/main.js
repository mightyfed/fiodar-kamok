function onLoad() {
	var editButtonTags = document.getElementsByClassName("editTag");
	var editButtonAuthors = document.getElementsByClassName("editAuthor");
	var tagIds = [];
	var authorIds = [];
	var tagText;
	var authorText;
	
	var cancelButtonTags = document.getElementsByClassName("cancel");
	var cancelButtonAuthors =  document.getElementsByClassName("cancel");
	
	for (var i = 0; i < editButtonTags.length; i++) {
	    listenerForEditButtonTagI( i );
	}
	
	for (var i = 0; i < editButtonAuthors.length; i++) {
		listenerForAuthorI ( i );
	}
	
	for (var i = 0; i < cancelButtonTags.length; i++) {
	    listenerForCancelButtonTagI( i );
	}
	
	for (var i = 0; i < cancelButtonAuthors.length; i++) {
	    listenerForCancelButtonAuthorI( i );
	}
	
	function listenerForEditButtonTagI(i) {
		
		tagIds[i] = editButtonTags[i].value;
		
		editButtonTags[i].addEventListener('click', function() {
			tagText = document.getElementById("tag" + tagIds[i]).value;
			for(var j = 0; j < tagIds.length; j++) {
				var e = document.getElementById("tag" + tagIds[j]);
				e.readOnly = true;
			}
			
			var e = document.getElementById("tag" + tagIds[i]);
			e.readOnly = false;
			var cancelButton = document.getElementById("cancel" + tagIds[i]);
			cancelButton.style.display = "inline-block";
			var editButton = document.getElementById("edit" + tagIds[i]);
			editButton.style.display = "none";
			
			
			for(var j = 0; j < tagIds.length; j++) {
				var e = document.getElementById("updateButton" + tagIds[j]);
				e.type = "hidden";
				var e = document.getElementById("deleteButton" + tagIds[j]);
				e.type = "hidden";
				
				if(i !== j) {
					var cancelButton = document.getElementById("cancel" + tagIds[j]);
					cancelButton.style.display = "none";
					var editButton = document.getElementById("edit" + tagIds[j]);
					editButton.style.display = "inline-block";
				}
			}
			
			var updateButton = document.getElementById("updateButton" + tagIds[i]);
			var deleteButton = document.getElementById("deleteButton" + tagIds[i]);
			
			updateButton.type="submit";
			deleteButton.type="submit";
	    }, false);	
	
	}
	
	function listenerForAuthorI(i) {
		authorIds[i] = editButtonAuthors[i].value;
		
		editButtonAuthors[i].addEventListener('click', function() {
			tagText = document.getElementById("author" + authorIds[i]).value;
			
			for(var j = 0; j < authorIds.length; j++) {
				var e = document.getElementById("author" + authorIds[j]);
				e.readOnly = true;
			}
			
			var e = document.getElementById("author" + authorIds[i]);
			e.readOnly = false;
			var cancelButton = document.getElementById("cancel" + authorIds[i]);
			cancelButton.style.display = "inline-block";
			var editButton = document.getElementById("edit" + authorIds[i]);
			editButton.style.display = "none";
			
			
			for(var j = 0; j < authorIds.length; j++) {
				var e = document.getElementById("updateButton" + authorIds[j]);
				e.type = "hidden";
				var e = document.getElementById("expiredButton" + authorIds[j]);
				e.type = "hidden";
				if(i !== j) {
					var cancelButton = document.getElementById("cancel" + authorIds[j]);
					cancelButton.style.display = "none";
					var editButton = document.getElementById("edit" + authorIds[j]);
					editButton.style.display = "inline-block";
				}
			}
			
			var updateButton = document.getElementById("updateButton" + authorIds[i]);
			var expiredButton = document.getElementById("expiredButton" + authorIds[i]);
			
			updateButton.type="submit";
			expiredButton.type="submit";
			
	    }, false);		
	}
	
	
	function listenerForCancelButtonTagI( i ) {
				
		cancelButtonTags[i].addEventListener('click', function(){  
			tagIds[i] = editButtonTags[i].value;
			var cancelButton = document.getElementById("cancel" + tagIds[i]);
			cancelButton.style.display = "none";
			var editButton = document.getElementById("edit" + tagIds[i]);
			editButton.style.display = "inline-block";
			var e = document.getElementById("updateButton" + tagIds[i]);
			e.type = "hidden";
			e = document.getElementById("deleteButton" + tagIds[i]);
			e.type = "hidden";
			e = document.getElementById("tag" + tagIds[i]);
			e.readOnly = true;
			document.getElementById("tag" + tagIds[i]).value = tagText;
		}) 
	}

	function listenerForCancelButtonAuthorI( i ) {
		cancelButtonAuthors[i].addEventListener('click', function(){  
			authorIds[i] = editButtonAuthors[i].value;
			var cancelButton = document.getElementById("cancel" + authorIds[i]);
			cancelButton.style.display = "none";
			var editButton = document.getElementById("edit" + authorIds[i]);
			editButton.style.display = "inline-block";
			var e = document.getElementById("updateButton" + authorIds[i]);
			e.type = "hidden";
			e = document.getElementById("expiredButton" + authorIds[i]);
			e.type = "hidden";
			e = document.getElementById("author" + authorIds[i]);
			e.readOnly = true;
			document.getElementById("author" + authorIds[i]).value = tagText;
		}) 
		
	}
	
}